//
//  QuizVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/2/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol QuizVCDelegate{
    func quizVc(nextButton vc:UIViewController)->Void
    func quizVc(previousButton vc:UIViewController)->Void
    func quizVc(selectedAnswer:Int,submitButton vc:UIViewController)->Void
}
