//
//  UpgradeVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/8/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class UpgradePageVC: UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate {

    private var viewModels:[FreeOrPremiumPresentVC.ViewModel]!
    private var targetIndex = 0
    private var currentIndex = 0
    
    var pageDelegate:UpgradePageVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModels = [
       FreeOrPremiumPresentVC.ViewModel(index: 0, image: UIImage(named: "test")!),
       FreeOrPremiumPresentVC.ViewModel(index: 1, image: UIImage(named: "test")!),
        ]

        dataSource = self
        delegate = self
        setViewControlller(viewController: getViewController(index: 0))
        currentIndex = 0
        
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        return getViewController(index: (viewController as! FreeOrPremiumPresentVC).viewModel.index+1)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return getViewController(index: (viewController as! FreeOrPremiumPresentVC).viewModel.index-1)
    }
    
    private func setViewControlller(viewController:UIViewController?,direction:UIPageViewControllerNavigationDirection = .forward)
    {
        if let vc = viewController {
            setViewControllers([vc], direction: direction, animated: true, completion: nil)
        }
        
    }

    
    private func getViewController(index:Int)->UIViewController?
    {
        if viewModels.count <= index || index < 0
        {
            return nil
        }
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: FREE_OR_PREMIUM_PRESENT_VC) as? FreeOrPremiumPresentVC
        {
            vc.viewModel =  viewModels[index]
            return vc
        }
        return nil
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, willTransitionTo pendingViewControllers: [UIViewController]) {
        targetIndex = (pendingViewControllers[0] as! FreeOrPremiumPresentVC).viewModel.index
    }
    
//    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if(currentIndex != targetIndex && completed)
        {
            currentIndex = targetIndex
            pageViewContrller(_pageChanged: currentIndex)
            
        }
        print("Did Finish Animationg:\(completed)")
        
    }
    
    func pageViewContrller(_pageChanged index:Int)
    {
        print("Page Changed:\(index)")
        pageDelegate?.upgradePageVC(pageChanged: index)
    }
    
    public func setTheCurrentIndex(index:Int)
    {
        if currentIndex != index {
            setViewControlller(viewController: getViewController(index: index),direction: currentIndex>index ? .reverse: .forward)
            currentIndex = index
            pageViewContrller(_pageChanged: currentIndex)
        }
       
    }


}
