//
//  Utils.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/16/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation
import MessageUI

public typealias Callback = ()->()
public typealias OnFinished = (AnyObject?)->()
public typealias OnTaskFinished = (Bool)->()

let FILTER_UPDATED_EVENT = Notification.Name("features_updated")
let PROGRESS_UPDATED_EVENT = Notification.Name("progress_updated")

let QUIZ_GROUP_VC_SEGUE = "QuizGroupVC"
let QUIZ_PAGE_VC = "QuizPageVC"
let FREE_OR_PREMIUM_PRESENT_VC = "FreeOrPremiumPresentVC"
let LOADING_VC_SEGUE = "LoadingVC"

let UPGRADE_PAGE_SEGUE = "UpgradePageVC"
let UPGRADE_VC_ID = "UpgradeVC"
let RESET_USER_PASSWORD_VC_ID = "ResetUserPasswordVC"

let APP_URL = "http://www.google.com"
let OWNER_EMAIL = "darsan1991@gmail.com"

var IS_UNLOCKED:Bool{
get{
   return UserDefaults.standard.value(forKey: "locked") as! Bool
}
set(newVlaue){
    UserDefaults.standard.setValue(newVlaue, forKey: "locked")
}
}

let REVEAL_SWIPE_EDGE_DETECT_DEFAULT = CGFloat(50)

func showUpgrade(viewController:UIViewController)
{
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: UPGRADE_VC_ID)
    viewController.present(vc, animated: true, completion: nil)
}

func showResetPassword(viewController:UIViewController)
{
    let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: RESET_USER_PASSWORD_VC_ID)
    viewController.present(vc, animated: true, completion: nil)
}


func getFiendlyTimeForSync(date:Date)->String
{
    let currentDate = Date()
    let timeInterval = currentDate.timeIntervalSince(date)
    
    let seconds = Int(timeInterval)
    
    if seconds >= 3600*24
    {
        return "\(seconds%(3600*24)) days ago"
    }
    
    if seconds >= 3600
    {
        return "\(seconds%(3600)) hours ago"
    }
    
    if seconds >= 60
    {
        return "\(seconds%(60)) minutes ago"
    }
    
    return "\(seconds) seconds ago"
    
    
}

func showActionSheetForQuestion(question:Question,delegate:MFMailComposeViewControllerDelegate,vc:UIViewController)
{

    var questionTxt = "\(question.question)"
    for i in 1...question.options.count
    {
        questionTxt = "\(questionTxt) \n \(i).\(question.options[i-1])"
    }

   let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
    let share = UIAlertAction(title: "Share With Friends", style: .default) { (action) in
        let text = "Check this Awasom Question \(questionTxt)"
               shareText(text:text , vc: vc)
    }
    
    let shareApp = UIAlertAction(title: "Share this App", style: .default) { (action) in
        let text = "Check this Awasom App \(APP_URL)"
        shareText(text:text , vc: vc)
    }
    
    let improve = UIAlertAction(title: "Improve This Question", style: .default) { (action) in
        
        let subject = "Improve Question"
        let body = questionTxt
        
        sendEmailToOwner(subject: subject, body: body,delegate:delegate,vc:vc)
        
    }
    
    let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
    
    actionSheet.addAction(share)
    actionSheet.addAction(shareApp)
    actionSheet.addAction(improve)
    actionSheet.addAction(cancel)
    vc.present(actionSheet, animated: true, completion: nil)
}

func sendEmailToOwner(subject:String,body:String,delegate:MFMailComposeViewControllerDelegate,vc:UIViewController) {
    if MFMailComposeViewController.canSendMail() {
        let mail = MFMailComposeViewController()
        mail.setSubject(subject)
        mail.setToRecipients([OWNER_EMAIL])
        mail.mailComposeDelegate = delegate
        mail.setMessageBody(body, isHTML: true)
        //            mail.mailComposeDelegate = self
        vc.present(mail, animated: true)
    } else {
        // show failure alert
    }
}



func shareText(text:String,vc:UIViewController)
{
    // set up activity view controller
    let textToShare = [ text ]
    let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
    activityViewController.popoverPresentationController?.sourceView = vc.view // so that iPads won't crash
    
    // exclude some activity types from the list (optional)
    activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
    
    // present the view controller
    vc.present(activityViewController, animated: true, completion: nil)
    
    
}

