//
//  DefinitionPageVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/31/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class DefinitionPageVC: UIPageViewController,UIPageViewControllerDataSource,UIPageViewControllerDelegate,DefinitionPageTileVCDelegate {
    
    var definitions:[Definition]!
    var startDefinition:Definition!

    override func viewDidLoad() {
        super.viewDidLoad()
        dataSource = self
       
        inilizeThePageAtStart()
        
        // Do any additional setup after loading the view.
    }
    
    private func inilizeThePageAtStart()
    {
        let startIndex = definitions.index { (definition) -> Bool in
            return definition.id == startDefinition.id
        }
        
        setViewControllers([getViewController(index: startIndex!)!], direction: .forward, animated: true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        return getViewController(index: (viewController as! DefinitionPageTileVC).definitionVM.index + 1)
        
    }
    
    private func getViewController(index:Int)->UIViewController?
    {
        if definitions.count <= index || index < 0
        {
            return nil
        }
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DefinitionPageTileVC") as? DefinitionPageTileVC
        {
            vc.definitionVM = DefinitionPageTileVC.DefinitionPageTileViewModel(definition: definitions[index], index: index, maxIndex: definitions.count-1)
            vc.delegate = self
            return vc
        }
        return nil
    }
    
    private func setViewControlller(viewController:UIViewController?,direction:UIPageViewControllerNavigationDirection = .forward)
    {
        if let vc = viewController {
            setViewControllers([vc], direction: direction, animated: true, completion: nil)
        }
        
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
    
        return  getViewController(index: (viewController as! DefinitionPageTileVC).definitionVM.index - 1)
        
    }
    
    
    
    func definitionPageTile(click_next_button_ tile: DefinitionPageTileVC) {
        if tile.definitionVM.index >= (definitions.count - 1)
        {
            return
        }
      setViewControlller(viewController: getViewController(index: tile.definitionVM.index+1))
    }
    
    func definitionPageTile(click_previous_button_ tile: DefinitionPageTileVC) {
        if tile.definitionVM.index <= 0
        {
            return
        }
        setViewControlller(viewController: getViewController(index: tile.definitionVM.index-1),direction: .reverse)
    }
    
    

}
