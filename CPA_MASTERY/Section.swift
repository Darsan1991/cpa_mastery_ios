//
//  Section.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class Section
{
    public private(set) var id:Int
    public private(set) var title:String
    public private(set) var subjectId:Int
    
    public private(set) var relatedQuestionIDs:[Int]
    public private(set) var isPremium:Bool

    public private(set) var relationQuestions:[Question]?
    public private(set) var subject:Subject?
    
    init(id:Int,title:String,subjectId:Int,relatedQuestionIds:[Int],isPremium:Bool) {
        self.id = id
        self.subjectId = subjectId
        self.title = title
        self.relatedQuestionIDs = relatedQuestionIds
        self.isPremium = isPremium
    }
    
    func setRelatedQuestions(relatedQuestions:[Question])
    {
        self.relationQuestions = relatedQuestions
    }
    
    func setSubject(subject:Subject)
    {
        self.subject = subject
    }
    
    //For Colom Identifier
    public static let ID = "Id"
    public static let TITLE = "Title"
    public static let PREMIUM = "Premium"
    public static let SUBJECT_ID = "SubjectId"

}
