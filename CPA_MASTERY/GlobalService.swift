//
//  GlobalService.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/24/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

public class GlobalService
{
    public static var instance = GlobalService()
    
    public private(set) var inilized:Bool!
    
    public private(set) var isPremium:Bool = false
    
    public private(set) var accountHandler:AccountHandler!
    
    func inilizeOrReferesh()
    {
        inilized = false
        isPremium = false
        accountHandler = FirebaseAccountHandler()
        accountHandler.isPremiumUnlocked { (isUnlock) in
            self.isPremium = isUnlock
            self.inilized = true
        }
    }
    
    
    
}
