//
//  Filter.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public enum Filter:Int
{
    case UN_ANSWRED = 0
    case DONT_KNOW = 1
    case SOME_HOW_KNOW = 2
    case KNOW = 3
    case BOOKMARK = 4
}

public func filterToProgress(filter:Filter) -> ProgressType
{
    switch filter {
    case .UN_ANSWRED:
        return ProgressType.UN_ANSWRED
        
    case .DONT_KNOW:
        return ProgressType.DONT_KNOW
        
    case .SOME_HOW_KNOW:
        return ProgressType.SOME_HOW_KNOW
        
    case .KNOW:
        return ProgressType.KNOW

    default:
        return ProgressType.UN_ANSWRED
    }
}
