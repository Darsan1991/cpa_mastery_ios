//
//  ProgressHeaderDetailsViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/25/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation
import UICircularProgressRing

public class ProgressHeaderDetailsViewCell:UITableViewCell
{
    @IBOutlet weak var cirularProgressBar:UICircularProgressRingView!

    
    @IBOutlet weak var unAnswredLabel:UILabel!
    @IBOutlet weak var dontKnowLabel:UILabel!
    @IBOutlet weak var someHowKnowLabel:UILabel!
    @IBOutlet weak var knowLabel:UILabel!
    @IBOutlet weak var bookMarkLabel:UILabel!
    @IBOutlet weak var scoreLable:UILabel!
    
    func configureCell(viewModel:ProgressHeaderDetailsViewModel)
    {
        
        cirularProgressBar.setProgress(value: viewModel.correctPercentage, animationDuration: 3)
        unAnswredLabel.text = "\(viewModel.getQuestionsCountForFiler(filter: Filter.UN_ANSWRED))"
        
        dontKnowLabel.text = "\(viewModel.getQuestionsCountForFiler(filter: Filter.DONT_KNOW))"
        
        someHowKnowLabel.text = "\(viewModel.getQuestionsCountForFiler(filter: Filter.SOME_HOW_KNOW))"
        
        knowLabel.text = "\(viewModel.getQuestionsCountForFiler(filter: Filter.KNOW))"
        
        bookMarkLabel.text = "\(viewModel.getQuestionsCountForFiler(filter: Filter.BOOKMARK))"
        
        scoreLable.text = "\(viewModel.answredCorrectly) / \(viewModel.totallyAnswred)"
        
    }
}

public class ProgressHeaderDetailsViewModel
{
    var correctPercentage:CGFloat
    {
        return totallyAnswred > 0 ? 100 * CGFloat(answredCorrectly)/CGFloat(totallyAnswred) : 0
    }
    
    var answredCorrectly:Int=0
    
    var totallyAnswred:Int
    {
        return questionAdapter.totalQuestionList.count - questionAdapter.getQuestionCount(progressType: ProgressType.UN_ANSWRED)
    }
    
    private var questionAdapter:QuestionProgressAdapter!
    private var bookMarkAdapter:BookMarkProgressAdapter!
    
    var isStrong:Bool{
        return answredCorrectly == totallyAnswred
    }
    
    var isWeak:Bool
    {
        return answredCorrectly == 0 && totallyAnswred != 0
    }

    init(questions:[QuestionProgress]) {
        questionAdapter = QuestionProgressAdapter(totalQuestions: questions)
         bookMarkAdapter = BookMarkProgressAdapter(totalQuestions: questions)
        
        answredCorrectly = 0
        
        for question in questions
        {
            if question.isAnswredCorrectly
            {
                answredCorrectly = answredCorrectly + 1
            }
        }
    }
    
    init(questionAdapter:QuestionProgressAdapter,bookMarkAdapter:BookMarkProgressAdapter,answredCorrectly:Int)
    {
        self.questionAdapter = questionAdapter
        self.bookMarkAdapter = bookMarkAdapter
        self.answredCorrectly = answredCorrectly
    }
    
    func getQuestionsCountForFiler(filter:Filter)->Int {
        if filter == Filter.BOOKMARK
        {
            return bookMarkAdapter.bookMarkQuestionList.count
        }
        
        return questionAdapter.getQuestionCount(progressType: filterToProgress(filter: filter))
    }
}
