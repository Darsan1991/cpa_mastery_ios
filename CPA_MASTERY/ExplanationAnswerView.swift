//
//  ExplanationAnswerView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/5/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ExplanationAnswerView: UIView {
    
    private static let correctIcon = "ic_correct"
    private static let wrongIcon = "ic_wrong"

    @IBOutlet weak var iconIV:UIImageView!
    @IBOutlet weak var answer:UILabel!
    
    var viewModel:ExplanationAnswerViewModel
    {
        set(newVal)
        {
            _viewModel = newVal
            refereshUI()
        }
        get{
            return _viewModel
        }
    }
    
    private var _viewModel:ExplanationAnswerViewModel!
    
   
    
    private func refereshUI()
    {
        if viewModel.state != .NONE
        {
            iconIV.image = UIImage(named:viewModel.state == State.CORRECT ? ExplanationAnswerView.correctIcon : ExplanationAnswerView.wrongIcon)
        }
        else{
            iconIV.image = nil
        }
        
        answer.text = viewModel.answer
        if viewModel.isSelected
        {
            answer.font = UIFont.boldSystemFont(ofSize: answer.font.pointSize)
        }
        
    }
    
    public enum State
    {
        case CORRECT
        case WRONG
        case NONE
    }
    
    public class ExplanationAnswerViewModel
    {
        var answer:String
        var state:ExplanationAnswerView.State
        var isSelected:Bool
        
        init(answer:String,state:ExplanationAnswerView.State,isSelected:Bool) {
            self.answer = answer
            self.state = state
            self.isSelected = isSelected
        }
    }
}
