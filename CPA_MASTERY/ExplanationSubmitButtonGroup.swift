//
//  ExplanationSubmitButtonGroup.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/6/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ExplanationSubmitButtonGroup: UIView {

    @IBOutlet weak var dontKnowIV:UIImageView!
    @IBOutlet weak var someHowKnowIV:UIImageView!
    @IBOutlet weak var knowIV:UIImageView!
    
    var delegate:ExplanationSubmitButtonGroupDelegate?
    
    
    override func awakeFromNib() {
        
    }
    
    @IBAction func onSubmitClicked(_ button:UIButton)
    {
        var selProgress = ProgressType.DONT_KNOW
        switch(button.tag)
        {
        case 0:
            selProgress = .DONT_KNOW
            break
            
        case 1:
            selProgress = .SOME_HOW_KNOW
            break
            
        case 2:
            selProgress = .KNOW
            break
            
        default:
            break
        }
        
        delegate?.explanationSubmitButtonGroup(submitClicked: selProgress)
    }

    
    @IBAction func onTouchDown(_ button:UIButton)
    {
        switch(button.tag)
        {
        case 0:
            applyClickEffect(image: getImageViewForProgressType(progressType: .DONT_KNOW))
            break
            
        case 1:
             applyClickEffect(image: getImageViewForProgressType(progressType: .SOME_HOW_KNOW))
            break
            
        case 2:
             applyClickEffect(image: getImageViewForProgressType(progressType: .KNOW))
            break
            
        default:
            break
        }
    }
    
    @IBAction func onTouchExit(_ button:UIButton)
    {
        clearClickEffect()
    }
    
    private func getImageViewForProgressType(progressType:ProgressType)->UIImageView
    {
        switch progressType {
        case .DONT_KNOW:
            return dontKnowIV
            
        case .SOME_HOW_KNOW:
            return someHowKnowIV
            
        case .KNOW:
            return knowIV
            
        default:
            return dontKnowIV
        }
    }

    private func applyClickEffect(image:UIImageView)
    {
        image.alpha = 0.6    }
    
    private func clearClickEffect()
    {
        dontKnowIV.alpha = 1
        someHowKnowIV.alpha = 1
        knowIV.alpha = 1
    }
    
}
