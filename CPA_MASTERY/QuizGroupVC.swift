//
//  QuizGroupVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/6/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class QuizGroupVC: UIViewController ,UIGestureRecognizerDelegate,QuizPageVCDelegate{

    @IBOutlet weak var navagationBar: UINavigationItem!
    var questions:[QuestionProgress]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("Reveal View Controller:\(revealViewController())")
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        configureRevalController()
    }
    
    private func configureRevalController()
    {
        revealViewController().setLeftEnable(true)
        revealViewController().setRightEnable(true)
        revealViewController().panGestureRecognizer().delegate = self
        view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
    }
    
    private func removeRevalController()
    {
        revealViewController().panGestureRecognizer().delegate = nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        removeRevalController()    }

    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
       let location = touch.location(in: view)
        
        if location.x > view.frame.width - REVEAL_SWIPE_EDGE_DETECT_DEFAULT || location.x < REVEAL_SWIPE_EDGE_DETECT_DEFAULT
        {
            return true
        }
        
        
        return false
    }
   

    func quizPageVC(question: Question, onQuizChanged vc: UIViewController) {
        navagationBar.title = question.section?.title
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == QUIZ_PAGE_VC
        {
            let vc = (segue.destination as! QuizPageVC)
            vc.questions = questions
            vc.delegate = self
        }
    }

    @IBAction func onClickTools(_ sender: Any) {
        revealViewController().rightRevealToggle(self)
    }
}
