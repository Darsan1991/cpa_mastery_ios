//
//  ViewController.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/16/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,ProfileVCDelegate{
    @IBOutlet weak var unlockNotifaction: UIView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var menuBtn: UIBarButtonItem!
    
    private var subjectProvider:SubjectProvider!
    
    private var activeHomeTiles = [HomeTileUI]()
    private var globalService = GlobalService.instance

    override func viewDidLoad() {
        super.viewDidLoad()
        subjectProvider = InformationService.instance.subjectProvider
        configureCollectionView()
        inilizeHomeTiles()
        
        unlockNotifaction.isHidden = globalService.isPremium
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        configureRevealController()
    }
    
    private func inilizeHomeTiles()
    {
   
        let subjects = subjectProvider.getAllSubjects()
        
        for subject in subjects
        {
            activeHomeTiles.append(HomeTileUI(iconImage: "subject_icon", title: subject.title, isPremium: subject.premium && !globalService.isPremium, homeType: HomeTileUI.HomeType.SUBJECT,extra:subject))
        }
        
        
        activeHomeTiles.append(HomeTileUI(iconImage: "definition_icon", title: "Definition", isPremium: false, homeType: HomeTileUI.HomeType.DEFINITION,extra:nil))
        
        activeHomeTiles.append(HomeTileUI(iconImage: "progress_icon", title: "Progress", isPremium: false, homeType: HomeTileUI.HomeType.PROGRESS,extra:nil))

        
    }
    
    private func configureCollectionView()
    {
        collectionView.delegate = self
        collectionView.dataSource = self
    }
    
    private func configureRevealController()
    {
        revealViewController().setRightEnable(false)
        revealViewController().setLeftEnable(true)
        
        menuBtn.target = revealViewController()
        menuBtn.action = #selector(revealViewController().revealToggle(_:))
        revealViewController().panGestureRecognizer().delegate = nil
        self.view.addGestureRecognizer(revealViewController().panGestureRecognizer())
        self.view.addGestureRecognizer(revealViewController().tapGestureRecognizer())
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       
        var vc = segue.destination
        
        if let nc = vc as? UINavigationController
        {
            vc = nc.viewControllers[0]
            if let profileVC = vc as? ProfileVC
            {
                profileVC.delegate = self
            }
        }
        
        
        if let recievable = vc as? SegueNameRecievable
        {
            recievable.segueNameRecievable(lastVCName: "Home")
        }
        
        if segue.identifier == "SubjectProgressVC"
        {
            (segue.destination as! SubjectProgressVC).subject = sender as! Subject
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       if let viewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCollectionViewCell", for: indexPath) as? HomeCollectionViewCell
       {
            viewCell.configureCell(tile: activeHomeTiles[indexPath.row])
            return viewCell
        }
        
        return HomeCollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return activeHomeTiles.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch activeHomeTiles[indexPath.row].homeType  {
        case .SUBJECT:
            
            if activeHomeTiles[indexPath.row].isPremium == true
            {
                showUpgrade(viewController: self)
            }
            else
            {
            performSegue(withIdentifier: "SubjectProgressVC", sender: activeHomeTiles[indexPath.row].extra)
            }
            break
            
        case .PROGRESS:
            performSegue(withIdentifier: "ProgressVC", sender: nil)
            break
            
        case .DEFINITION:
            performSegue(withIdentifier: "DefinitionGroupVC", sender: nil)
            break
            
        default:
            break
        }
        
       
        
    }
    
    func profileVC(onLogOut vc: UIViewController) {
        print("OnLogOut")
       navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func profileVC(onSync vc: UIViewController) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func profileVC(onReset vc: UIViewController) {
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickShowMe(_ sender: Any) {
        showUpgrade(viewController: self)
    }
}

