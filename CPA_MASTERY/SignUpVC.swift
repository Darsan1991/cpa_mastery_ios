//
//  SignUpViewController.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/17/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit
import FirebaseAuth

class SignUpVC: UIViewController,UITextFieldDelegate {
    @IBOutlet weak var createAccountBtn: ButtonD!
    
    @IBOutlet weak var usernameTxtField: TextFieldD!
    @IBOutlet weak var emailTxtField: TextFieldD!
    @IBOutlet weak var passwordTxtField: TextFieldD!
    
    private var accountHandler:AccountHandler!
    private var progressHandler = ProgressHandler()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        usernameTxtField.addTarget(self, action: #selector(SignUpVC.textFieldDidChange), for: UIControlEvents.editingChanged)
        
        emailTxtField.addTarget(self, action: #selector(SignUpVC.textFieldDidChange), for: UIControlEvents.editingChanged)
        
        passwordTxtField.addTarget(self, action: #selector(SignUpVC.textFieldDidChange), for: UIControlEvents.editingChanged)
        
        accountHandler = FirebaseAccountHandler()
        
        //Fake call for Referesh
        textFieldDidChange()

        // Do any additional setup after loading the view.
    }

    @IBAction func onCreateAccountClicked(_ sender: Any) {
        progressHandler.show()
      accountHandler.createAccount(email: emailTxtField.text!, password: passwordTxtField.text!) { (error) in
        self.progressHandler.dismiss()
        if error != nil{
            self.onCreatedAccount()
        }
        else
        {
            print("Account Created Successfully")
        }
    }
    }
    
    private func onCreatedAccount()
    {
        performSegue(withIdentifier: LOADING_VC_SEGUE, sender: nil)
    }
    
    @IBAction func onBackClicked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    func textFieldDidChange()
    {
        if emailTxtField.text != nil && emailTxtField.text!.characters.count > 0 && usernameTxtField.text != nil && usernameTxtField.text!.characters.count > 0 &&
            passwordTxtField.text != nil && passwordTxtField.text!.characters.count > 0
        {
            if !createAccountBtn.isUserInteractionEnabled
            {
                createAccountBtn.isUserInteractionEnabled = true
            }
        }
        else
        {
            if createAccountBtn.isUserInteractionEnabled
            {
                createAccountBtn.isUserInteractionEnabled = false
            }
        }
    }
    
}
