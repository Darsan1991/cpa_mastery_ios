//
//  SegueNameRecievable.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/17/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol SegueNameRecievable{
    
    
    // Display customization
    
    func segueNameRecievable(lastVCName:String)

}
