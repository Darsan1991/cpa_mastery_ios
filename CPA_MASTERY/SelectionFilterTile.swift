//
//  SelectionFilterTile.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class SelectionFilterTile: UIView {
    
    private var selectedColor:UIColor{
        get{
            return filterBtn.borderColor
        }
    }
    
    @IBOutlet weak var filterBtn:ButtonD!
    var delegate:SelectionFilterTileDelegate?
    
    private var _isSelected:Bool = false
    
    @IBAction func onClick(sender:UIButton)
    {
        if let del = delegate
        {
            del.onClick(selectiongTile: self)
        }
    }
    
    var isSelected:Bool{
        set(value){
            _isSelected = value
            filterBtn.backgroundColor = _isSelected ? selectedColor:UIColor.clear
        }
        get{
            return _isSelected
        }
    }

    
}
