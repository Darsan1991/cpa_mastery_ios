//
//  QuestionProgressDto.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation


public class QuestionProgressDto
{
    var id:Int
    var isBookMarked:Bool
    var progressType:Int
    var isCorrect:Bool
    
    public init(id:Int, isBookMarked:Bool, progressType:Int,isCorrect:Bool) {
    self.id = id
    self.isBookMarked = isBookMarked
    self.progressType = progressType
    self.isCorrect = isCorrect
    }
}
