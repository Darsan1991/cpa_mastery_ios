//
//  QuizQuestionViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/3/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

public class QuizQuestionViewCell: UITableViewCell {

    @IBOutlet weak var questionLabel:UILabel!
    @IBOutlet weak var progresBar:MultiProgressBar!
    @IBOutlet weak var subjectTitleLabel:UILabel!
    @IBOutlet weak var imgView: UIImageView!
    @IBOutlet weak var indexLabel:UILabel!
    
    var delegate:QuizQuestionViewCellDelegate?
    
    var viewModel:QuizQuestionViewCellModel{
        get{
            return _viewModel
        }
        set(newVal){
            _viewModel = newVal
            configureCell()
        }
    }
    
    private var _viewModel:QuizQuestionViewCellModel!
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none

    }
    
    
    private func configureCell()
    {
        questionLabel.text = viewModel.question.question
        progresBar.setAdapterAndShowAllProgress(adapter: viewModel.progressAdapter)
        imgView.image = UIImage(named:viewModel.question.imageUrl!)
        subjectTitleLabel.text = viewModel.question.section?.title
        indexLabel.text = "\(viewModel.index+1) of \(viewModel.maxIndex+1)"
    }
    
    @IBAction func onClickAction(_ sender:UIButton)
    {
        delegate?.quizQuestionViewCell(actionButton: self)
    }


}

public class QuizQuestionViewCellModel
{
    var question:QuestionProgress
    var progressAdapter:QuestionProgressAdapter
    var index:Int
    var maxIndex:Int
    
    init(question:QuestionProgress,progressAdapter:QuestionProgressAdapter,index:Int,maxIndex:Int) {
        self.question = question
        self.index = index
        self.maxIndex = maxIndex
        self.progressAdapter = progressAdapter
    }
}
