//
//  QuestionProgressProvider.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/24/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation


public protocol QuestionProgressProvider
{
    var lastSynced:Date{get}
    func getQuestionsForIds(idList:[Int])->[QuestionProgress?]
    func getQuestionForId(id:Int)->QuestionProgress?
    func getQuestionList()->[QuestionProgress]
    func updateQuestionProgressType(questionId:Int,progressType:ProgressType)
    func updateBookMarked(questionId:Int,isBookMark:Bool)
    func updateAnsweredCorrectly(questionId:Int,isAnsweredCorrectly:Bool)
}
