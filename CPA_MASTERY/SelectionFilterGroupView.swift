//
//  SelectionFilterGroupView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

@IBDesignable public class SelectionFilterGroupView : UIView,SelectionFilterTileDelegate
{
    
    @IBOutlet weak var unAnswredFilter:SelectionFilterTile!
    @IBOutlet weak var dontKnowFilter:SelectionFilterTile!
    @IBOutlet weak var someHowKnowFilter:SelectionFilterTile!
    @IBOutlet weak var knowFilter:SelectionFilterTile!
    @IBOutlet weak var bookMarkFilter:SelectionFilterTile!
    @IBOutlet weak var quickStartBtn: ButtonD!
    @IBOutlet weak var selectedFilterTitleLabel: UILabel!
    
    var delegate:SelectionTileGroupViewDelegate?
    {
        get{
            return _delegate
        }
        
        set(value)
        {
            _delegate = value
            let adapters = value?.filterAdapters()
            questionAdapter = adapters?.0
            bookMarksAdapter = adapters?.1
            inilize(questionAdapter: questionAdapter, bookMarkAdapter: bookMarksAdapter)
        }
    }
    var selectedFilter:Filter{
        set(value)
        {
            _selectedFilter = value
            setAsSeletectedTile(selectiongTile: filterToTile(filter: _selectedFilter))
        }
        
        get
        {
            return _selectedFilter
        }
    }
    
    private var _selectedFilter:Filter!
    
    private var _delegate:SelectionTileGroupViewDelegate?
    
    private var selectedFilterTile:SelectionFilterTile!
    private var questionAdapter:QuestionProgressAdapter!
    private var bookMarksAdapter:BookMarkProgressAdapter!
    
   
    
    public override func awakeFromNib() {
        super.awakeFromNib()
       unAnswredFilter.delegate = self
        dontKnowFilter.delegate = self
        someHowKnowFilter.delegate = self
        knowFilter.delegate = self
        bookMarkFilter.delegate = self
        
        selectedFilterTile = bookMarkFilter
        selectedFilterTile = unAnswredFilter
        selectedFilterTile.isSelected = true
        
        
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    private func inilize(questionAdapter:QuestionProgressAdapter,bookMarkAdapter:BookMarkProgressAdapter)
    {
        bookMarkAdapter.addDatachangedListner {
            self.refereshUis()
            }
        
        questionAdapter.addDatachangedListner {
            self.refereshUis()
        }
        refereshUis()
    }
    
    func initSubviews() {
        // standard initialization logic
//        let bundle = Bundle(for: type(of: self))
//        let nib = UINib(nibName: "SelectionFilterGroupView", bundle: bundle)
//        let contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    public func onClick(selectiongTile: SelectionFilterTile) {
        
        if self.selectedFilterTile == selectiongTile
        {
            return
        }
        
        setAsSeletectedTile(selectiongTile: selectiongTile)
        
        if let del = delegate{
            del.onFilterButtonClicked(filter: tileToFilter(tile: self.selectedFilterTile))
        }
        
    }
    
    private func setAsSeletectedTile(selectiongTile: SelectionFilterTile)
    {
        _selectedFilter = tileToFilter(tile: selectiongTile)
        self.selectedFilterTile.isSelected = false
        self.selectedFilterTile = selectiongTile
        self.selectedFilterTile.isSelected = true
        let questions = getSelectedQuestions()
        quickStartBtn.setTitle("Quick Start \(questions.count) Questions",for: .normal)
        quickStartBtn.isUserInteractionEnabled = questions.count != 0
        selectedFilterTitleLabel.text = "Filter - \(getTitleForFilter(filter: tileToFilter(tile: selectedFilterTile)))"
    }
    
    @IBAction func onQuickStartButtonClicked(_ sender: Any) {
        if let del = delegate
        {
           
            del.onQuickStartClicked(questionProgresses: getSelectedQuestions())
        }
    }
    
    private func getSelectedQuestions()->[QuestionProgress]
    {
        return selectedFilterTile == bookMarkFilter ? bookMarksAdapter.bookMarkQuestionList : questionAdapter.getQuestions(progressType:  filterToProgress(filter:(tileToFilter(tile: selectedFilterTile))))
    }
    
    private func refereshUis()
    {
        if let questionAdapter = self.questionAdapter,let bookMarksAdapter = self.bookMarksAdapter
        {
        unAnswredFilter.filterBtn.setTitle("\(questionAdapter.getQuestions(progressType: ProgressType.UN_ANSWRED).count)", for: .normal)
        dontKnowFilter.filterBtn.setTitle("\(questionAdapter.getQuestions(progressType: ProgressType.DONT_KNOW).count)", for: .normal)
        someHowKnowFilter.filterBtn.setTitle("\(questionAdapter.getQuestions(progressType: ProgressType.SOME_HOW_KNOW).count)", for: .normal)
        knowFilter.filterBtn.setTitle("\(questionAdapter.getQuestions(progressType: ProgressType.KNOW).count)", for: .normal)
        bookMarkFilter.filterBtn.setTitle("\(bookMarksAdapter.bookMarkQuestionList.count)", for: .normal)
        }
    }
    
    private func filterToTile(filter:Filter) -> SelectionFilterTile
    {
        switch filter {
        case .BOOKMARK:
            return bookMarkFilter
            
        case .DONT_KNOW:
            return dontKnowFilter
            
        case .SOME_HOW_KNOW:
            return someHowKnowFilter
            
        case .KNOW:
            return knowFilter
            
        case .UN_ANSWRED:
            return unAnswredFilter
            
        }
    }
    
    private func tileToFilter(tile:SelectionFilterTile) -> Filter
    {
        switch tile {
        case unAnswredFilter:
            return Filter.UN_ANSWRED
            
        case dontKnowFilter:
            return Filter.DONT_KNOW
            
        case someHowKnowFilter:
            return Filter.SOME_HOW_KNOW
            
        case knowFilter:
            return Filter.KNOW
            
        case bookMarkFilter:
            return Filter.BOOKMARK
        default:
            return Filter.UN_ANSWRED
        }
    }
    
    public func getTitleForFilter(filter:Filter)->String
    {
        switch filter {
        case Filter.UN_ANSWRED:
            return "Unanswred"
            
        case Filter.DONT_KNOW:
            return "Don't Know"
            
        case Filter.SOME_HOW_KNOW:
            return "SomeWhat Know"
            
        case Filter.KNOW:
            return "Know"
            
        case Filter.BOOKMARK:
            return "BookMarked"
        }
    }
}
