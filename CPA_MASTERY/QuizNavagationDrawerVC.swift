//
//  QuizNavagationDrawerVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/10/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit
import MessageUI

class QuizNavagationDrawerVC: UITableViewController,MFMailComposeViewControllerDelegate {
    
    private static let bookmarkIndicator = "ic_star"
    private static let unBookmarkIndicator = "ic_star_blank"
    
    public static var instance:QuizNavagationDrawerVC!
    
    @IBOutlet weak var bookmarkLabel: UILabel!
    @IBOutlet weak var bookMarkIndicatorIV: UIImageView!
    
    var delegate:QuizNavagationDrawerVCDelegate?
    
    
    private var isInit = false
    
    
    

    var questionProgress:QuestionProgress!
    {
        didSet{
            refereshBookMarkUIs()
        }
    }
    
   private  func refereshBookMarkUIs()
   {
    if !isInit
    {
        return
    }
    
        bookmarkLabel.text = questionProgress.isBookMarked ?
        "UnBookmark This Question" : "Bookmark this question"
    bookMarkIndicatorIV.image = UIImage(named: questionProgress.isBookMarked ? QuizNavagationDrawerVC.bookmarkIndicator : QuizNavagationDrawerVC.unBookmarkIndicator)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        tableView.contentInset = UIEdgeInsetsMake(-20, 0.0, 0.0, 0.0)
        QuizNavagationDrawerVC.instance = self
        isInit = true
        refereshBookMarkUIs()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        QuizNavagationDrawerVC.instance = self
        print("Init Right")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
         QuizNavagationDrawerVC.instance = self
        print("Init Right")
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1
        {
            switch indexPath.row
            {
            case 0:
                onClickBackToCategory()
                break
                
            case 1:
                onClickBookMark()
                break
                
            case 2:
                onClickTalkExpert()
                break
                
            default:
                break
            }
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    private func onClickBackToCategory()
    {
        revealViewController().rightRevealToggle(animated: true)
        delegate?.quizNavagationDrawer(onBackToCategoryClick: self)
    }
    
    private func onClickBookMark()
    {
        delegate?.quizNavagationDrawer(onClickBookMark: !questionProgress.isBookMarked, vc: self)
        refereshBookMarkUIs()
    }
    
    private func onClickTalkExpert()
    {
        sendEmailToOwner(subject: "Talk to expert", body: "Write here", delegate: self, vc: self)
    }

    
}
