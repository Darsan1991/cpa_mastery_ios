//
//  SubjectProgressViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/23/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class SubjectProgressViewCell: UITableViewCell {
    
    private static var notActiveTitleColor = UIColor.gray
    private static var activeTitleColor = UIColor.black
    
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var questionCountLabel:UILabel!
    @IBOutlet weak var lockImageView:UIImageView!
    @IBOutlet weak var nextImageView:UIImageView!
    @IBOutlet weak var progressBar:MultiProgressBar!
    
    public private(set) var viewModel:SubjectProgressViewModel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        inilize()
    }
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        inilize()
    }
    
    private func inilize()
    {
        NotificationCenter.default.addObserver(self, selector: #selector(onFilterSelectionUpdated), name: FILTER_UPDATED_EVENT, object: nil)
    }
    
    @objc private  func onFilterSelectionUpdated()
    {
        configureCell(cellModel: viewModel)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
    }

    func configureCell(cellModel:SubjectProgressViewModel)
    {
        viewModel = cellModel
        titleLabel.text = cellModel.section.title
        lockImageView.isHidden = !cellModel.isLocked
        nextImageView.isHidden = cellModel.isLocked || cellModel.getActiveQuestions().count == 0
        let questionCount = cellModel.getActiveQuestions().count
        questionCountLabel.text = "\(questionCount) Questions"
        
        titleLabel.textColor = (questionCount == 0 || cellModel.isLocked) ? SubjectProgressViewCell.notActiveTitleColor : SubjectProgressViewCell.activeTitleColor
        if cellModel.currentFilter == Filter.BOOKMARK
        {
            progressBar.setAdapterAndShowAllProgress(adapter: cellModel.bookMarkAdapter)
        }
        else if cellModel.currentFilter == Filter.UN_ANSWRED
        {
            progressBar.setAdapterAndShowAllProgress(adapter: cellModel.questionAdapter)
        }
        else{
            progressBar.setAdapterAndShowOneProgress(adapter: cellModel.questionAdapter, type: filterToProgress(filter: cellModel.currentFilter))
        }
        
        
//        _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { (timer) in
//            self.progressBar.showOnlyOneProgress(type: ProgressType.DONT_KNOW)
//        })
    }

}

public class SubjectProgressViewModel
{
    public private(set) var section:Section
   public private(set) var questionAdapter:QuestionProgressAdapter
   public private(set) var bookMarkAdapter:BookMarkProgressAdapter
    var isLocked:Bool
    var currentFilter:Filter
    
    init(section:Section,questionAdapter:QuestionProgressAdapter,bookMarkAdapater:BookMarkProgressAdapter,isLocked:Bool,filter:Filter) {
        self.section = section
        self.questionAdapter = questionAdapter
        self.bookMarkAdapter = bookMarkAdapater
        self.isLocked = isLocked
        self.currentFilter = filter
    }
    
    func getActiveQuestions() -> [QuestionProgress]
    {
      return currentFilter != Filter.BOOKMARK ? questionAdapter.getQuestions(progressType: filterToProgress(filter: currentFilter)) : bookMarkAdapter.bookMarkQuestionList
    }
    
}
