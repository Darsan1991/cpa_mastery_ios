//
//  ExplanationTopQuizView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/4/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ExplanationTopQuizView: UIView {

    @IBOutlet weak var indicatiorLabel: UILabel!

    @IBOutlet weak var sectionLabel: UILabel!

    @IBOutlet weak var questionLabel: UILabel!
    
    @IBOutlet weak var progressBar: MultiProgressBar!
    var viewModel:ExplanationTopQuizViewModel?
    
    override func awakeFromNib() {
        indicatiorLabel.text = viewModel?.indicatorText
        sectionLabel.text = viewModel?.sectionText
        questionLabel.text = viewModel?.questionText
        if viewModel != nil
        {
        progressBar.setAdapterAndShowAllProgress(adapter: viewModel!.progressBar)
        }

    }
    
    func initSubviews() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        initSubviews()
    }
    
    init(frame:CGRect,vm:ExplanationTopQuizViewModel) {
        super.init(frame:frame)
        initSubviews()
        self.viewModel = vm
    }
 
    public class ExplanationTopQuizViewModel
    {
        var indicatorText:String
        var sectionText:String?
        var questionText:String
        var progressBar:QuestionProgressAdapter
        
        init(indicatorText:String,sectionText:String?,questionText:String,progressBar:QuestionProgressAdapter) {
            self.indicatorText = indicatorText
            self.progressBar = progressBar
            self.sectionText = sectionText
            self.questionText = questionText
        }
    }
    
}



