//
//  DefinitionViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/29/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class DefinitionViewCell:UITableViewCell
{
    @IBOutlet weak var titleLabel:UILabel!
    public private(set) var definition:Definition!
    
    func configureCell(definition:Definition)
    {
        self.definition = definition
        titleLabel.text = definition.title
    }
}
