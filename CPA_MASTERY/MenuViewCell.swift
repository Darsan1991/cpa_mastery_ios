//
//  MenuViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/16/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class MenuViewCell: UITableViewCell {

    @IBOutlet weak var label:UILabel!
    @IBOutlet weak var iconImg:UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(menuTile:MenuTile){
        label.text = menuTile.title
        
        if let name = menuTile.image
        {
            iconImg.image = UIImage(named: name)
        }
        else{
            iconImg.isHidden = true
        }
    }


}
