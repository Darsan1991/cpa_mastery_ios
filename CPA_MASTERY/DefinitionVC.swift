//
//  DefinitionVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/29/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class DefinitionVC:UIViewController,UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate
{
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var searchBar:UISearchBar!
    var avaliableDefinition:[Definition]!
    private var activeDefinitions:[Definition]!
    
    public override func viewDidLoad() {
//        avaliableDefinition = [Definition]()
//        avaliableDefinition.append(Definition(id: 1, title: "Definition 1", context: ""))
//        avaliableDefinition.append(Definition(id: 2, title: "Definition 2", context: ""))
//        avaliableDefinition.append(Definition(id: 3, title: "Definition 3", context: ""))
//        avaliableDefinition.append(Definition(id: 4, title: "Definition 4", context: ""))
//        avaliableDefinition.append(Definition(id: 5, title: "Definition 5", context: ""))
//        avaliableDefinition.append(Definition(id: 6, title: "Definition 6", context: ""))
//        avaliableDefinition.append(Definition(id: 7, title: "Definition 7", context: ""))
        
        activeDefinitions = avaliableDefinition
        
        hideKeyboardWhenTappedAround()
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        
    }
    
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "DefinitionViewCell") as? DefinitionViewCell
        {
            cell.configureCell(definition: activeDefinitions[indexPath.row])
            return cell
        }
        
        return UITableViewCell()
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activeDefinitions.count
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openDefinitionPage(selectedDefinition: activeDefinitions[indexPath.row])
    }
    
    public func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
    }
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if(searchText.characters.count==0)
        {
            activeDefinitions = avaliableDefinition
        }
        else
        {
        
       activeDefinitions = avaliableDefinition.filter { (definition) -> Bool in
            return definition.title.lowercased().contains(searchText.lowercased())
        }
        }
        tableView.reloadData()
    }
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
    
    private func openDefinitionPage(selectedDefinition:Definition)
    {
        performSegue(withIdentifier: "DefinitionPageVC", sender: selectedDefinition)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DefinitionPageVC" && sender != nil
        {
            let vc = segue.destination as! DefinitionPageVC
            vc.definitions = avaliableDefinition
            vc.startDefinition = sender as! Definition
        }
    }
    
}
