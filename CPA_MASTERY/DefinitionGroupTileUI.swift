//
//  DefinitionGroupUI.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/29/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class DefinitionGroupTileUI{
    var title:String!
    var definitions:[Definition]!
    var isLock:Bool!
    
    init(title:String,definitions:[Definition],isLock:Bool) {
        self.definitions = definitions
        self.isLock = isLock
        self.title = title
    }
    
}
