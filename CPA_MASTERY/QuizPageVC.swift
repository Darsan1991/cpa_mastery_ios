//
//  QuizPageVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/2/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class QuizPageVC: UIPageViewController,UIPageViewControllerDataSource,QuizAndAnswerParentVCDelegate {

    var questions:[QuestionProgress]!
    var questionProgressProvider:QuestionProgressProvider = QuestionProgressService.instance
    var questionProgressAdapter:QuestionProgressAdapter!

    var selectedIndex:Int!
    
    var quizDelegate:QuizPageVCDelegate?{
        return delegate != nil ? delegate! as? QuizPageVCDelegate : nil
    }
    
    var questionProgressAdapter:QuestionProgressAdapter!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
<<<<<<< HEAD
        questions = questionProgressProvider.getQuestionList()
=======
//        questions = questionProgressProvider.getQuestionList()
>>>>>>> master_1
        questionProgressAdapter = QuestionProgressAdapter(totalQuestions: questions)
        dataSource = self
        setViewControlller(viewController: getViewController(index: 0))
        disableScrollView()

        
    }
    
    private func disableScrollView()
    {
        for v in view.subviews
        {
            if v is UIScrollView
            {
                (v as! UIScrollView).isScrollEnabled = false
            }
        }

    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        return getViewController(index: (viewController as! QuizAndAnswerParentVC).viewModel.index + 1)
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        return getViewController(index: (viewController as! QuizAndAnswerParentVC).viewModel.index - 1)
    }
    
    

    
    

    
    
    
    private func setViewControlller(viewController:UIViewController?,direction:UIPageViewControllerNavigationDirection = .forward)
    {
        if let vc = viewController as? QuizAndAnswerParentVC {
            selectedIndex = vc.viewModel.index
            quizDelegate?.quizPageVC(question: questions[selectedIndex], onQuizChanged: self)
            setViewControllers([vc], direction: direction, animated: true, completion: nil)
        }
        
    }
    
    

    

    private func getViewController(index:Int)->UIViewController?
    {
        if questions.count <= index || index < 0
        {
            return nil
        }
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "QuizAndAnswerParentVC") as? QuizAndAnswerParentVC
        {
            vc.viewModel = QuizAndAnswerParentVC.QuizAndAnswerParentVCViewModel(questionProgress: questions[index],questionProgressAdapter:questionProgressAdapter, index: index, maxIndex: questions.count-1)
            vc.delegate = self
            return vc
        }
        return nil
    }
    
    func quizAndAnswerParentVC(clickedNextButton vc: UIViewController) {
        let index = (vc as! QuizAndAnswerParentVC).viewModel.index + 1
        setViewControlller(viewController: getViewController(index: index))
    
    }
    
    func quizAndAnswerParentVC(clickedPreviousButton vc: UIViewController) {
        let index = (vc as! QuizAndAnswerParentVC).viewModel.index - 1
        setViewControlller(viewController: getViewController(index: index),direction: .reverse)
    }
    
    func quizAndAnswerParentVC(progress: ProgressType, selectedProgress vc: UIViewController) {
        
        let vm = (vc as! QuizAndAnswerParentVC).viewModel!
        questionProgressProvider.updateQuestionProgressType(questionId: vm.questionProgress.id, progressType: progress)
        let index = vm.index + 1
        if index > questions.count - 1
        {
//            view.removeFromSuperview()
//            removeFromParentViewController()
//            dismiss(animated: true, completion: nil)
            navigationController?.popViewController(animated: true)
        }
        else
        {
            setViewControlller(viewController: getViewController(index: index))
        }
        questionProgressAdapter.referesh()
    }
    
    func quizAndAnswerParentVC(bookMark isBookMark: Bool, questionProgress: QuestionProgress, vc: UIViewController) {
        questionProgressProvider.updateBookMarked(questionId: questionProgress.id, isBookMark: isBookMark)
    }

}
