//
//  DefinitionGroupVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/29/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class DefinitionGroupVC:UIViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet weak var tableView:UITableView!
    
    private var premiumHeaderSize = CGFloat(25)
    private var isLocked = false
    
    private var definitionGroupList = [DefinitionGroupTileUI]()
    
    private let definitionProvider = InformationService.instance.definitionProvider
    
    public override func viewDidLoad() {
        isLocked = !GlobalService.instance.isPremium
        inilizeList()
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    private func inilizeList()
    {
        
        var defnitionsGroups = [DefinitionGroup]()
        defnitionsGroups.append(DefinitionGroup(startCharacter: "A", endCharacter: "C"))
        defnitionsGroups.append(DefinitionGroup(startCharacter: "D", endCharacter: "F"))
        defnitionsGroups.append(DefinitionGroup(startCharacter: "G", endCharacter: "L"))
        defnitionsGroups.append(DefinitionGroup(startCharacter: "M", endCharacter: "O"))
        defnitionsGroups.append(DefinitionGroup(startCharacter: "P", endCharacter: "R"))
        defnitionsGroups.append(DefinitionGroup(startCharacter: "S", endCharacter: "Z"))

        let definitions = definitionProvider.getDefinitionList()
        
        for def in definitions
        {
            for group in defnitionsGroups
            {
                if group.contains(character: def.title.characters.first!)
                {
                    group.definitions.append(def)
                    break
                }
            }
        }
        
        

        for g in defnitionsGroups
        {
            definitionGroupList.append(DefinitionGroupTileUI(title: g.title, definitions: g.definitions, isLock: false))
        }
        
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 && isLocked
        {
            let textWidth = CGFloat(100)
            let lineHeigt = CGFloat(1)
            let headerHeight = premiumHeaderSize
            let lineColor = UIColor.gray
            let bgColor = UIColor.lightGray
            
            let header = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: headerHeight))
            header.backgroundColor = bgColor
            
            let text = UILabel(frame: CGRect(x: view.frame.width/2 - textWidth/2, y: 0, width: textWidth, height: headerHeight))
            text.text = "PREMIUM"
            text.textAlignment = .center
            text.backgroundColor = UIColor.clear
            text.textColor = UIColor.gray
            text.font = UIFont(name: text.font.fontName, size: 13)
            header.addSubview(text)
            
            
            let leftLine = UIView(frame:CGRect(x:0,y:(headerHeight - lineHeigt)/2,width:(view.frame.width - textWidth)/2,height:lineHeigt))
            leftLine.backgroundColor = lineColor
            
            let rightLine = UIView(frame:CGRect(x:(view.frame.width + textWidth)/2,y:(headerHeight - lineHeigt)/2,width:(view.frame.width - textWidth)/2,height:lineHeigt))
            rightLine.backgroundColor = lineColor
            
            header.addSubview(leftLine)
            header.addSubview(rightLine)
            
            return header
        }
        return nil
    }

    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 && isLocked
        {
            return premiumHeaderSize
        }
        return 0
    }
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if let cell = tableView.dequeueReusableCell(withIdentifier: "DefinitionGroupViewCell") as? DefinitionGroupViewCell
       {
            cell.configureCell(definitionGroupVM: definitionGroupList[indexPath.row])
        
            return cell
        }
        
        return UITableViewCell()
    }
    
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return definitionGroupList.count
        default:
            return 0
        }
    }
    
    public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section==0)
        {
            let definition = definitionGroupList[indexPath.row]
            if !definition.isLock
            {
                openDefinitionViewController(definitions: definition.definitions)
            }
            else{
                showPremiumPopup()
            }
        }
    }
    
    private func openDefinitionViewController(definitions:[Definition])
    {
        performSegue(withIdentifier: "DefinitionVC", sender: definitions)
    }
    
    public override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "DefinitionVC" && sender != nil
        {
            let vc = segue.destination as! DefinitionVC
            vc.avaliableDefinition = sender as! [Definition]

        }
    }
    
    @IBAction func onClickSearch(_ sender: Any) {
        var defs = [Definition]()
        for defGroup in definitionGroupList
        {
            if !defGroup.isLock
            {
                defs.append(contentsOf: defGroup.definitions)
            }
        }
        openDefinitionViewController(definitions: defs)
        
    }
    
    private func showPremiumPopup()
    {
       if let v = UIApplication.shared.keyWindow
       {
        PopUpHandler(rootView: v, vc: self).show()
        }
    }
    
    private class DefinitionGroup
    {
        var startCharacter:Character
        var endCharacter:Character
        
        var title:String{
            return "\(startCharacter) to \(endCharacter)"
        }
        
        var definitions = [Definition]()
        
        init(startCharacter:Character,endCharacter:Character) {
            self.startCharacter = startCharacter
            self.endCharacter = endCharacter
        }
        
        public func contains(character : Character)->Bool
        {
            var charcaterInt = characterToInt(character: character)
            return charcaterInt >= characterToInt(character: startCharacter) && charcaterInt <= characterToInt(character: endCharacter)
        }
        
        private func characterToInt(character:Character)->Int
        {
            return Int(String(character).unicodeScalars.map{ $0.value }.reduce(0, +))
        }
        
        
    }
}
