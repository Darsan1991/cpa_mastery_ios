//
//  SingleProgressBar.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/31/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class SingleProgressBar: UIView {
    @IBInspectable var barBackgroundColor:UIColor = UIColor.lightGray
    @IBInspectable var barColor:UIColor = UIColor.white

    
//    let barBackgroundColor = UIColor.lightGray
        private var backgroundView:UIView?
    private var barView:UIView?
    private var isInilize:Bool = false

    public var maxProgress:CGFloat = 100{
        didSet{
            refereshProgress()
        }
    }
    
    public var progress:CGFloat = 10{
        didSet{
            refereshProgress()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //background view
        if isInilize
        {
            return
        }
        backgroundView = UIView()
        backgroundView!.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: frame.height)
        
        backgroundView!.center = CGPoint(x: frame.width/2, y: frame.height/2)
        backgroundView!.clipsToBounds = true
        backgroundView!.backgroundColor = barBackgroundColor
        self.addSubview(backgroundView!)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if isInilize
        {
            return
        }
        
        backgroundView!.frame = CGRect(x: 0, y: 0, width: frame.width, height: backgroundView!.frame.height)
        backgroundView!.layer.cornerRadius = frame.height/2
        backgroundView!.layoutIfNeeded()

        barView = UIView(frame: CGRect(x:0,y:0,width:0,height:backgroundView!.frame.height))
        barView!.backgroundColor = barColor
        backgroundView!.addSubview(barView!)
        refereshProgress()
    }

    private func refereshProgress()
    {
       let width = (backgroundView?.frame.width)! * progress/maxProgress
        barView?.frame = CGRect(x:0,y:0,width:width,height:(barView?.frame.height)!)
    }
    

}
