//
//  ProfileVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/17/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ProfileVC: UITableViewController ,SegueNameRecievable{

    @IBOutlet weak var syncTxt: UILabel!
    @IBOutlet weak var emailTxt: UILabel!
    @IBOutlet weak var menuOrbackBtn: UIBarButtonItem!
    
    private var lastVCName:String?
    
    private var accountHandler:AccountHandler = FirebaseAccountHandler()
    private var progressHandler = ProgressHandler()
    private var questionProvider:QuestionProgressProvider!
    
    var delegate:ProfileVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        questionProvider = QuestionProgressService.instance
        
        if let name = lastVCName
        {
            menuOrbackBtn.title = name
            menuOrbackBtn.target = self
            menuOrbackBtn.action = #selector(onClickBack(_:))
        }
        else
        {
            menuOrbackBtn.title = "Menu"
            menuOrbackBtn.target = revealViewController()
            menuOrbackBtn.action = #selector(revealViewController().revealToggle(_:))
        }
        
        emailTxt.text = accountHandler.getEmailAddress()
        syncTxt.text = getFiendlyTimeForSync(date:questionProvider.lastSynced)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func segueNameRecievable(lastVCName: String) {
        self.lastVCName = lastVCName
    }
    
    @objc private func onClickBack(_:Any)
    {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onClickResetPassword(_ sender: Any) {
       
        showResetPassword(viewController: self)
        
    }

    @IBAction func onClickSyncProgress(_ sender: Any) {
        

        self.navigationController?.dismiss(animated: true, completion: {
            self.delegate?.profileVC(onSync: self)
        })

    }
    
    @IBAction func onClickReset(_ sender: Any) {
        
        let vc = UIAlertController(title: "Are You Sure Reset?", message: "This will delete your progress you will start from begning", preferredStyle: .alert)
        let action = UIAlertAction(title: "Reset", style:.default) { (action) in
            
            self.progressHandler.show()
            
            self.accountHandler.resetAccount { (sucess) in
                self.progressHandler.dismiss()
                self.navigationController?.dismiss(animated: true, completion: {
                    self.delegate?.profileVC(onReset: self)
                })

            }
            
        }
        
         let cancel = UIAlertAction(title: "Cancel", style:.cancel)
        
        vc.addAction(action)
        vc.addAction(cancel)
        present(vc, animated: true, completion: nil)
        
    }
    
    @IBAction func onClickLogOut(_ sender: Any) {
       let vc = UIAlertController(title: "Are You Sure Want to LogOut?", message: "You can log in any time.The Progress is saved", preferredStyle: .alert)
        let action = UIAlertAction(title: "LogOut", style:.default) { (action) in
             self.accountHandler.signOut()
           
            self.navigationController?.dismiss(animated: true, completion: {
                 self.delegate?.profileVC(onLogOut: self)
            })
        }
        
        let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        vc.addAction(action)
        vc.addAction(cancel)
        
        present(vc, animated: true, completion: nil)
        
       
    }
    
}
