//
//  LogInViewController.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/17/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit
import FirebaseAuth

class LogInVC: UIViewController {

    @IBOutlet weak var emailTxtField: TextFieldD!
    @IBOutlet weak var passwordTxtField: TextFieldD!
    @IBOutlet weak var loginWithFacebookBtn: ButtonD!
    
    private var accountHandler:AccountHandler!
    private var progressHandler = ProgressHandler()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        accountHandler = FirebaseAccountHandler()
        emailTxtField.addTarget(self, action: #selector(LogInVC.textFieldDidChange), for: UIControlEvents.editingChanged)
        
        passwordTxtField.addTarget(self, action: #selector(LogInVC.textFieldDidChange), for: UIControlEvents.editingChanged)
        
        //Fake call for Referesh
        textFieldDidChange()
    }


    @IBAction func onClickFacebook(_ sender: Any) {
        
        let loginManager = LoginManager()
        loginManager.logIn([ ReadPermission.publicProfile ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                
                print("Logged in!")
                self.onSignInFacebook()
            }
        }
        
    }
    @IBAction func onClickLoginInEmailButton(_ sender: Any) {
      
        let progress = ProgressHandler()
        progress.show()
        accountHandler.signInWithEmail(email: emailTxtField.text!, password: passwordTxtField.text!) { (error) in
            progress.dismiss()
            if error != nil{
                print("Error In Login")
            }
            else
            {
                self.onSignedInToAccount()
            }
        }
      
    }

    
    private func onSignInFacebook()
    {
        let progress = ProgressHandler()
        progress.show()
        accountHandler.signInAccountWithFacebook(accessToken: FBSDKAccessToken.current().tokenString) { (error) in
            progress.dismiss()
            if error != nil{
                print("Error In Sinn in")
            }
            else
            {
               self.onSignedInToAccount()
            }
        }

    
    }
    
    private func onSignedInToAccount()
    {
        performSegue(withIdentifier: LOADING_VC_SEGUE, sender: nil)
        print("user Successfully sign in:\(Auth.auth().currentUser?.uid)")

    }
    
    func textFieldDidChange()
    {
        if emailTxtField.text != nil && emailTxtField.text!.characters.count > 0 &&
            passwordTxtField.text != nil && passwordTxtField.text!.characters.count > 0
        {
            if !loginWithFacebookBtn.isUserInteractionEnabled
            {
                loginWithFacebookBtn.isUserInteractionEnabled = true
            }
        }
        else
        {
            if loginWithFacebookBtn.isUserInteractionEnabled
            {
                loginWithFacebookBtn.isUserInteractionEnabled = false
            }
            
        }

        
    }


}
