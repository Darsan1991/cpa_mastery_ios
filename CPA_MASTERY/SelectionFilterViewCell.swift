//
//  SelectionFilterViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/23/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class SelectionFilterViewCell: UITableViewCell {

    @IBOutlet weak var selectionFilterGroup:SelectionFilterGroupView!
    
    var delegate:SelectionFilterViewCellDelegate?
    {
        get{
            return _delegate
        }
        
        set(value)
        {
            _delegate = value
            selectionFilterGroup.delegate = value
        }
    }
    
    private var _delegate:SelectionFilterViewCellDelegate?
    public var selectedFilter:Filter{
        get
        {
        return selectionFilterGroup.selectedFilter
        }
        set(val)
        {
            selectionFilterGroup.selectedFilter = val
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
    }
    


}
