//
//  QuizPageVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/21/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol QuizPageVCDelegate:UIPageViewControllerDelegate
{
    func quizPageVC(question:Question, onQuizChanged vc:UIViewController)->Void
}
