//
//  ExplanationVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/2/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol ExplanationVCDelegate
{
    func explanationVC(progressType:ProgressType, selectProgress vc:UIViewController)
}
