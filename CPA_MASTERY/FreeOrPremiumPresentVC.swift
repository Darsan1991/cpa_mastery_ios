//
//  FreeOrPremiumPresentVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/8/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class FreeOrPremiumPresentVC: UIViewController {
    @IBOutlet weak var image: AutoHeightImageView!

    
    
    var viewModel:ViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func onClickRestore(_ sender: Any) {
       
    }
    

    public class ViewModel
    {
        var index:Int
        var image:UIImage
        
        init(index:Int,image:UIImage) {
            self.image = image
            self.index = index
        }
    }

}
