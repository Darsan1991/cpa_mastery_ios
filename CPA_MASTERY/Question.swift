//
//  Question.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class Question
{
    public private(set) var id:Int
    public private(set) var question:String
    public private(set) var options:[String]
    public private(set) var correctAnswerIndex:Int
    public private(set) var imageUrl:String?
    public private(set) var explanation:String
    public private(set) var sectionId:Int
    
    public private(set) var section:Section?
    
    init(id:Int,question:String,options:[String],correctAnswerIndex:Int,imageUrl:String?,explanation:String,sectionId:Int) {
        
        self.id = id
        self.question = question
        self.options = options
        self.correctAnswerIndex = correctAnswerIndex
        self.imageUrl = imageUrl
        self.explanation = explanation
        self.sectionId = sectionId
    }
    
    public func setSection(section:Section)
    {
        self.section = section
    }
    
    
    //For Colom Identifier
    public static let ID = "Id"
    public static let ANSWER_INDEX = "AnswerIndex"
    public static let EXPLANATION = "Explanation"
    public static let IMAGE_URL = "ImageUrl"
    public static let OPTIONS = "Options"
    public static let QUESTION_CONTEXt = "QuestionContext"
    public static let SECTION_ID = "SectionId"
    
    
    
}
