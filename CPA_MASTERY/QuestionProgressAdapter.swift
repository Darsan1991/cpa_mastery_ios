//
//  QuestionProgressAdapter.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class QuestionProgressAdapter:ProgressAdapter
{
    
    override init(totalQuestions: [QuestionProgress], onDataChanged: Callback? = nil) {
        super.init(totalQuestions: totalQuestions,onDataChanged: onDataChanged)
        
        inilize()
    
    }
    
    private func inilize()
    {
        for i in 0...3
        {
            typeVsProgressDict.updateValue([QuestionProgress](), forKey: ProgressType(rawValue: i)!)
        }
        
        for progress in totalQuestionList
        {
            typeVsProgressDict[progress.progressType]!.append(progress)
        }
    }
    
    override func referesh()
    {
        typeVsProgressDict.removeAll()
        
        inilize()
        dataChanged()
        
//        var isDataChanged=false;
//        
//        for i in 0...3
//        {
//            let type = ProgressType(rawValue:i)!
//            var list = typeVsProgressDict[type]!
//            
//            var index = 0;
//            while index < list.count {
//                let progress = list[index]
//                if type != progress.progressType
//                {
//                    if !isDataChanged
//                    {
//                        isDataChanged = true
//                    }
//                    typeVsProgressDict[progress.progressType]!.append(progress)
//                    list.remove(at: index)
//                }
//                else
//                {
//                    index = index + 1
//                }
//            }
//            
//            
//        }
//        
//        if isDataChanged
//        {
//            dataChanged()
//        }
    }

}
