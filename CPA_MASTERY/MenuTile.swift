//
//  MenuTile.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/16/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class MenuTile{
    var title:String!
    var image:String?
    var type:MenuType
    
    init(title:String,image:String?,menuType:MenuType) {
        self.title = title
        self.image = image
        self.type = menuType
    }

}

public enum MenuType{
    case HOME
    case WHAT_IS_UPGRADE
    case TALK_TO_EXPERT
    case PROFILE
}
