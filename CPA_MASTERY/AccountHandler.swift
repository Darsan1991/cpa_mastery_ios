//
//  AccountHandler.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol AccountHandler
{
    typealias OnPremiumUnlock = (Bool)->()
    func createAccount(email:String,password:String,onFinished:OnFinished?)->Void
    func signInAccountWithFacebook(accessToken:String,onFinished:OnFinished?)->Void
    func signInWithEmail(email:String,password:String,onFinished:OnFinished?)->Void
    func resetPassword(email:String,onFinished:OnFinished?)->Void
    func isAccountLogin()->Bool
    func isPremiumUnlocked(premiumUnlock:OnPremiumUnlock?)->Void
    func unlockPremiumLock(unlock:Bool,finished:OnTaskFinished?)->Void
    func resetAccount(taskFinished:@escaping OnTaskFinished)->Void
    func getEmailAddress()->String
    func updatePassword(newPassword:String,taskFinished:OnTaskFinished?)
    func signOut()->Void

}
