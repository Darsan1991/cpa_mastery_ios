//
//  ProfileVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/24/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol ProfileVCDelegate
{
    func profileVC(onLogOut vc:UIViewController)->Void
    func profileVC(onSync vc:UIViewController)->Void
    func profileVC(onReset vc:UIViewController)->Void
}
