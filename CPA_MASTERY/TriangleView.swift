//
//  TriangleView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/25/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

@IBDesignable class TriangleView: UIView {
    override func draw(_ rect: CGRect) {
        let width:CGFloat = bounds.width/9
        let height:CGFloat = bounds.width/9
        
        let path = UIBezierPath()
        path.move(to: bounds.origin)
        path.addLine(to: CGPoint(x: width, y: bounds.origin.y))
        path.addLine(to: CGPoint(x: bounds.origin.x, y: height))
        // closePath will connect back to start point
        path.close()
        
        let shape = CAShapeLayer()
        shape.path = path.cgPath
        shape.fillColor = UIColor.blue.cgColor
        
        self.layer.insertSublayer(shape, at: 0)
        
        // Setting text margin
//        textContainerInset = UIEdgeInsetsMake(8, width, 8, 0)
    }
}
