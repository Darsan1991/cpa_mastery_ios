//
//  ProgressType.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public enum ProgressType:Int
{
    case UN_ANSWRED = 0
    case DONT_KNOW = 1
    case SOME_HOW_KNOW = 2
    case KNOW = 3
}
