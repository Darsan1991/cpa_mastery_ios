//
//  SelectionTileGroupViewDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol SelectionTileGroupViewDelegate
{
    func onFilterButtonClicked(filter:Filter)
    func onQuickStartClicked(questionProgresses:[QuestionProgress])
    func filterAdapters()->(QuestionProgressAdapter,BookMarkProgressAdapter)
}
