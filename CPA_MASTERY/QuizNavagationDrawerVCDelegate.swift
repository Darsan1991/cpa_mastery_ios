//
//  QuizNavagationDrawerVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/10/17.
//  Copyright © 2017 fiverr. All rights reserved.
//



protocol QuizNavagationDrawerVCDelegate {
    func quizNavagationDrawer(onBackToCategoryClick vc:UIViewController)
    func quizNavagationDrawer(onClickBookMark bookMark:Bool, vc:UIViewController)
}
