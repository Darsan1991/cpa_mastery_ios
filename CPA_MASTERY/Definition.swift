//
//  Definition.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class Definition
{
    public private(set) var id:Int
    public private(set) var title:String
    public private(set) var context:String
    
    init(id:Int,title:String,context:String) {
        self.id = id
        self.title = title
        self.context = context
    }
    
    
    //For Colom Identifier
    public static let ID = "Id"
    public static let TITLE = "Title"
    public static let CONTEXT = "Context"
}
