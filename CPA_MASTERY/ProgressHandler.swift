//
//  ProgressHandler.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/23/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation


public class ProgressHandler
{
    private var rootView:UIView!
    
    public  private(set) var progress:UIView!
    
    init()
    {
        if let v = UIApplication.shared.keyWindow
        {
            self.rootView = v
        }
        inilize()
    }
    
    init(rootView:UIView) {
        self.rootView = rootView
        inilize()
    }
    
    private func inilize()
    {
         progress = Progress(frame: CGRect(x: 0, y: 0, width: rootView.frame.width, height: rootView.frame.height))
    }
    
    public func show()
    {
        rootView.addSubview(progress)
    }
    
    public func dismiss()
    {
        progress.removeFromSuperview()
    }
}
