//
//  ProgressVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/25/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ProgressVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    
    @IBOutlet weak var tableView:UITableView!
    
    private var progressHeaderViewModel:ProgressHeaderDetailsViewModel!
    private var progresses = [ProgressViewModel]()
    
    private let informationProvider = InformationService.instance
    private let questionProvider = QuestionProgressService.instance
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        progressHeaderViewModel = ProgressHeaderDetailsViewModel(questions: QuestionProgressService.instance.getQuestionList())
        
        inilizeProgressModels()
        tableView.delegate = self
        tableView.dataSource = self
        
    }
    
    private func inilizeProgressModels()
    {
        let sections = informationProvider.sectionProvider.getSectionList()
        for sec in sections{
            progresses.append(ProgressViewModel(title: sec.title,questionProgresses: questionProvider.getQuestionsForIds(idList: sec.relatedQuestionIDs) as! [QuestionProgress]))
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
            
        case 1:
            return progresses.count
        default:
            return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 220
        default:
            return 60
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProgressHeaderDetailsViewCell") as? ProgressHeaderDetailsViewCell
            {
                cell.configureCell(viewModel: progressHeaderViewModel)
                return cell
            }
            break
        case 1:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "ProgressViewCell") as? ProgressViewCell
            {
                cell.configureCell(viewModel: progresses[indexPath.row])
                return cell
            }
            break
            
        default: break
           
        }
         return UITableViewCell()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
}
