//
//  AutoHeightImageView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/8/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class AutoHeightImageView: UIImageView {

    override var image: UIImage?{
        didSet
        {
            referesh()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        
    }
    
    override func awakeFromNib() {
//        referesh()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        referesh()
         print("width\(superview!.frame.width)")
    }
    
    func referesh()
    {
        if let i = image
        {
            
            let height = (superview?.frame.width)! * i.size.height / i.size.width
            print("width\(superview!.frame.width)")
           translatesAutoresizingMaskIntoConstraints = false
            
//
            let margin = superview!.layoutMarginsGuide
            topAnchor.constraint(equalTo: margin.topAnchor).isActive = true
            leftAnchor.constraint(equalTo: margin.leftAnchor).isActive = true
            rightAnchor.constraint(equalTo: margin.rightAnchor).isActive = true
             heightAnchor.constraint(equalToConstant: height).isActive = true
//
//            updateConstraints()
            layoutIfNeeded()
//            setNeedsDisplay()
//            heightAnchor.constraint(equalTo: height).
            
        }
        
    }

}
