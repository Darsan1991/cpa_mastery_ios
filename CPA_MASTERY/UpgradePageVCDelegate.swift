//
//  UpgradePageVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/8/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

protocol UpgradePageVCDelegate {
    func upgradePageVC(pageChanged index:Int)
}
