//
//  ExplanationAnswerViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/4/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ExplanationAnswerViewCell: UITableViewCell {
    private static var correctIconName = "ic_correct"
     private static var wrongIconName = "ic_wrong"

    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var answerLabel:UILabel!
    
    var viewModel:ExplanationAnswerViewCellModel
    {
        set(newValue)
        {
            _viewModel = newValue
            configureCell(vm: _viewModel)
        }
        get
        {
            return _viewModel
        }
    }
    
    private var _viewModel:ExplanationAnswerViewCellModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    private func configureCell(vm:ExplanationAnswerViewCellModel)
    {
        if vm.state != State.NONE
        {
            iconView.image = UIImage(named: vm.state == State.CORRECT ? ExplanationAnswerViewCell.correctIconName : ExplanationAnswerViewCell.wrongIconName)
        }
        else
        {
            iconView.image = nil
        }
        
        answerLabel.text = vm.answer
    }

    
    public enum State
    {
        case CORRECT
        case WRONG
        case NONE
    }

}


class ExplanationAnswerViewCellModel
{
    var state:ExplanationAnswerViewCell.State
    var answer:String
    
    init(answer:String,state:ExplanationAnswerViewCell.State) {
        self.state = state
        self.answer = answer
    }
}
