//
//  FirbaseAccountHandler.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

public class FirebaseAccountHandler:AccountHandler{
    
    public func createAccount(email:String,password:String,onFinished:OnFinished?=nil)
    {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            if error == nil
            {
                
            Database.database().reference().child("users").child(Auth.auth().currentUser!.uid).setValue(["Premium":false], withCompletionBlock: { (error, ref) in
                
                if let finished = onFinished
                {
                    finished(error == nil ? nil : error as AnyObject)
                }
            })
                
            }
            else if let finished = onFinished
            {
                finished(error == nil ? nil : error as AnyObject)
            }
        }
    }
    
    public func signInAccountWithFacebook(accessToken:String,onFinished:OnFinished?=nil)
    {
        let credential = FacebookAuthProvider.credential(withAccessToken: accessToken)
        
        Auth.auth().signIn(with: credential) { (user, error) in
            
            if error == nil
            {
                Database.database().reference().child("users").child(Auth.auth().currentUser!.uid).observeSingleEvent(of: .value, with: { (snapshot) in
                    
                    var alredyHaveAccount = false
                    if let dict = snapshot.value as? NSDictionary
                    {
                        alredyHaveAccount = dict["Premium"] != nil
                    }
                    
                    if !alredyHaveAccount
                    {
                        Database.database().reference().child("users").child(Auth.auth().currentUser!.uid).setValue(["Premium":false], withCompletionBlock: { (error, ref) in
                            
                            print("Creating User")
                            if let finished = onFinished
                            {
                                finished(error == nil ? nil : error as AnyObject)
                            }
                            
                        })
                    }
                    else
                    {
                        if let finished = onFinished
                        {
                            finished(error == nil ? nil : error as AnyObject)
                        }
                    }
                    
                    
                })
            }
            else
            {
                if let finished = onFinished
                {
                    finished(error as AnyObject)
                }
            }
            
            
        }

    }
    
    public func signInWithEmail(email:String,password:String,onFinished:OnFinished?=nil)
    {
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            
            if let finished = onFinished
            {
                finished(error == nil ? nil : error as AnyObject)
            }
        }
    }
    
    public func resetPassword(email: String, onFinished: OnFinished?) {
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            if let finished = onFinished
            {
                finished(error == nil ? nil : error as AnyObject)
            }
        }
    }
    
    public func isAccountLogin() -> Bool {
        return Auth.auth().currentUser != nil
    }
    
    
    public func signOut()
    {
        do
            {
                try Auth.auth().signOut()
        }
        catch
        {
            
        }
    }
    

    
    public func updatePassword(newPassword:String,taskFinished:OnTaskFinished?)
    {
        Auth.auth().currentUser!.updatePassword(to: newPassword) { (err) in
            taskFinished?(err == nil)
        }
    }
    
    public func getEmailAddress()->String
    {
       return Auth.auth().currentUser!.email!
    }
    
    public func resetAccount(taskFinished:@escaping OnTaskFinished)
    {
        FirebaseQuestionPresistance().reset(onTaskFinished: taskFinished)
    }
    
    public func isPremiumUnlocked(premiumUnlock: AccountHandler.OnPremiumUnlock?) {
        Database.database().reference().child("users").child(Auth.auth().currentUser!.uid).child("Premium").observeSingleEvent(of: .value, with: { (snap) in
               premiumUnlock?(snap.value as! Bool)
        })
    }
    
    public func unlockPremiumLock(unlock:Bool,finished:OnTaskFinished?)
    {
        Database.database().reference().child("users").child(Auth.auth().currentUser!.uid).child("Premium").setValue(unlock) { (err, ref) in
            finished?(err == nil)
        }
    }

}

