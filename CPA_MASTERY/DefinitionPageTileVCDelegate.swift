//
//  DefinitionPageTileVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/31/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

protocol DefinitionPageTileVCDelegate
{

     func definitionPageTile(click_next_button_ tile: DefinitionPageTileVC)
    func definitionPageTile(click_previous_button_ tile: DefinitionPageTileVC)
}
