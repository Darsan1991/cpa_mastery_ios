//
//  StackView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/3/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class StackView: UIStackView {
    private var color: UIColor?
   override var backgroundColor: UIColor? {
        get { return color }
        set {
            color = newValue
            self.setNeedsLayout() 
        }
    }
    
    private lazy var backgroundLayer: CAShapeLayer = {
        let layer = CAShapeLayer()
        self.layer.insertSublayer(layer, at: 0)
        return layer
    }()
    override func layoutSubviews() {
        super.layoutSubviews()
        backgroundLayer.path = UIBezierPath(rect: self.bounds).cgPath
        backgroundLayer.fillColor = self.backgroundColor?.cgColor
    }
}
