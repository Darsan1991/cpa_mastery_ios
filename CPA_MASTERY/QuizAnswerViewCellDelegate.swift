//
//  QuizAnswerViewCellDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/3/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

protocol QuizAnswerViewCellDelegate {
    func quizAnswerViewCell(toggleCross cell:QuizAnswerViewCell)->Void
}
