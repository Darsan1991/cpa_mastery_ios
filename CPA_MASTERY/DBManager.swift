//
//  DBManager.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/11/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation
import FMDB

public class DBManager
{
    public static var instance:DBManager = DBManager()
    private var database:FMDatabase!
    init() {
        let path = Bundle.main.path(forResource: "Information", ofType: "db")!
        database = FMDatabase(path: path)
    }
    
    public func loadDefinitions()->[Definition]
    {
        var definitions = [Definition]()
        if database!.open()
        {
            
            let query = "select * from definitions"
            do {
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    
                    
                    let definition = Definition(
                        id: Int(results.int(forColumn: Definition.ID))
                        , title: results.string(forColumn: Definition.TITLE)
                        , context: results.string(forColumn: Definition.CONTEXT)
                    )
                    
                    definitions.append(definition)
                    
                }
            }
            catch {
                print(error.localizedDescription)
            }
            
        }
        return definitions
    }
    
    public func loadSubjects()->[Subject]
    {
      var subjects = [Subject]()
            if database!.open()
            {
                
                let query = "select * from subjects"
                do {
                    let results = try database.executeQuery(query, values: nil)

                    while results.next() {
                        let sectionQuery = "select * from sections where \(Section.SUBJECT_ID) ==  \(results.int(forColumn: Subject.ID))"
                        let sections = try database.executeQuery(sectionQuery, values: nil)
                        var sectionsIds = [Int]()
                        while sections.next()
                        {
                            sectionsIds.append(Int(sections.int(forColumn: Section.ID)))
                        }
                        
                        let subject = Subject(id: Int(results.int(forColumn: Subject.ID))
                            , title: results.string(forColumn: Subject.TITLE)
                            , sectionIds: sectionsIds
                            , premium: results.int(forColumn: Subject.PREMIUM) == 1
                        )
                        
                        subjects.append(subject)
                        
                    }
                }
                catch {
                    print(error.localizedDescription)
                }
                
            }
        return subjects
    }
    
    public func loadQuestions()->[Question]
    {
        var questions = [Question]()
        if database!.open()
        {
            
            let query = "select * from Questions"
            do {
                let results = try database.executeQuery(query, values: nil)
                while results.next() {
                    let options = results.string(forColumn: Question.OPTIONS).components(separatedBy: ",")
                    let question = Question(id: Int(results.int(forColumn: Question.ID))
                        , question: results.string(forColumn: Question.QUESTION_CONTEXt)
                        , options: options
                        , correctAnswerIndex: Int(results.int(forColumn: Question.ANSWER_INDEX))
                        , imageUrl: results.string(forColumn: Question.IMAGE_URL)
                        , explanation: results.string(forColumn: Question.EXPLANATION)
                        , sectionId: Int(results.int(forColumn: Question.SECTION_ID))
                    )
                    questions.append(question)
                }
               
                
            }
            catch
            {
                
            }
        }
        
            return questions

    }
    
    public func loadSections()->[Section]
    {
        var sections = [Section]()
        if database!.open()
        {
            
            let query = "select * from Sections"
            do {
                let results = try database.executeQuery(query, values: nil)
                
                while results.next() {
                    let questionQuery = "select * from Questions Where \(Question.SECTION_ID)==\(results.int(forColumn: Section.ID))"
                    let questions = try database.executeQuery(questionQuery, values: nil)
                    var questionIds = [Int]()
                    while questions.next() {
                        questionIds.append(Int(questions.int(forColumn: Question.ID)))
                    }
                    
                    let section = Section(id: Int(results.int(forColumn: Section.ID)),
                                          title: results.string(forColumn: Section.TITLE),
                                          subjectId: Int(results.int(forColumn: Section.SUBJECT_ID)),
                                          relatedQuestionIds: questionIds,
                                          isPremium: results.int(forColumn: Section.PREMIUM) == 1
                    )
                    
                    sections.append(section)
                }
                
                
                
            }
            catch
            {
                
            }
        }
        
        return sections

    }
        
        

}

