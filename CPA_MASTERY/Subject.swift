//
//  Subject.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class Subject
{
    public private(set) var id:Int
    public private(set) var title:String
    public private(set) var sectionIds:[Int]
    public private(set) var sections:[Section]?
    public private(set) var premium:Bool
    
    public init(id:Int,title:String,sectionIds:[Int],premium:Bool)
    {
        self.id = id
        self.title = title
        self.sectionIds = sectionIds
        self.premium = premium
    }
    
    func setSections(sections:[Section]){
        self.sections = sections
    }
    

    
    //For Colom Identifier
    public static let ID = "Id"
    public static let TITLE = "Title"
    public static let PREMIUM = "Premium"
    
}
