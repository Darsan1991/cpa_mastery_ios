//
//  MenuViewController.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/16/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit
import MessageUI

class MenuViewController: UIViewController,MFMailComposeViewControllerDelegate ,UITableViewDelegate ,UITableViewDataSource{

    private static let firstControllerID = "HomeViewController"
    
    @IBOutlet weak var tableView:UITableView!
    
    var activeList = [MenuTile]()
    
    private let globalService = GlobalService.instance
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        activeList.append(MenuTile(title: "Home", image: nil, menuType: .HOME))
        if !globalService.isPremium
        {
            activeList.append(MenuTile(title: "What's in the Upgrade?", image: nil, menuType: MenuType.WHAT_IS_UPGRADE))
        }
        activeList.append(MenuTile(title: "Talk to Expert", image: nil, menuType: MenuType.TALK_TO_EXPERT))
        activeList.append(MenuTile(title: "Profile", image: "profile_icon", menuType: MenuType.PROFILE))
        
        
        tableView.delegate = self
        tableView.dataSource = self

        // Do any additional setup after loading the view.
    }
    
    
    private func closeOrOpenMenu() {
        revealViewController().revealToggle(self)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return activeList.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       if let menuCell = tableView.dequeueReusableCell(withIdentifier: "MenuViewCell") as? MenuViewCell
       {
            menuCell.configureCell(menuTile: activeList[indexPath.row])
            return menuCell
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let tile = activeList[indexPath.row]
        onItemClick(menuType: tile.type)
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    private func onItemClick(menuType:MenuType)
    {
        switch menuType {
        case .HOME:
            openInFront(controllerId: "HomeViewController")
            break
            
        case .PROFILE:
            openInFront(controllerId: "ProfileVC")
            break
            
        case .WHAT_IS_UPGRADE:
            showUpgrade(viewController: self)
            break
            
        case .TALK_TO_EXPERT:
            sendEmailToOwner(subject: "Talk to expert", body: "write your  text here....", delegate: self, vc: self)
            break

        }
    }
    
    
    private func openInFront(controllerId:String)
    {
        let mainstoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        let newFrontController = mainstoryboard.instantiateViewController(withIdentifier: "HomeNavagationController") as! UINavigationController
        
        if controllerId != MenuViewController.firstControllerID
        {
        let newViewcontroller = mainstoryboard.instantiateViewController(withIdentifier: controllerId)
            newFrontController.pushViewController(newViewcontroller, animated: false)
        }
        
        revealViewController().pushFrontViewController(newFrontController, animated: false)
        closeOrOpenMenu()
    }

}
