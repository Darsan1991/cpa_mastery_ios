//
//  QuestionProgressService.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/24/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class QuestionProgressService:QuestionProgressProvider
{
    public private(set) static var instance = QuestionProgressService()
    public private(set) var questionList = [QuestionProgress]()
    
    private var questionDict = Dictionary<Int,QuestionProgress>()
    private let informationProvider:InformationProvider = InformationService.instance
    private var questionPresistance:QuestionProgressPresistance!
    
<<<<<<< HEAD
    init() {
        questionList.append(QuestionProgress(id: 1, question: "Question 1", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "Hellow World how are you whre are you from how do u do what is this nosonse this Darsan This is game Hellow World how are you whre are you from how do u do what is this nosonse this Darsan This is game Hellow World how are you whre are you from how do u do what is this nosonse this Darsan This is game", sectionId: 1, progressType: ProgressType.DONT_KNOW, isBookMarked: false, isAnswredCorrectly: false))
        
        questionList.append(QuestionProgress(id: 2, question: "Question 2", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 1, progressType: ProgressType.KNOW, isBookMarked: false, isAnswredCorrectly: false))
        
        questionList.append(QuestionProgress(id: 3, question: "Question 3", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 1, progressType: ProgressType.SOME_HOW_KNOW, isBookMarked: true, isAnswredCorrectly: true))
        
        questionList.append(QuestionProgress(id: 4, question: "Question 4", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 1, progressType: ProgressType.UN_ANSWRED, isBookMarked: false, isAnswredCorrectly: false))
        
        questionList.append(QuestionProgress(id: 5, question: "Question 5", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 2, progressType: ProgressType.KNOW, isBookMarked: true, isAnswredCorrectly: true))
        
        questionList.append(QuestionProgress(id: 6, question: "Question 6", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 2, progressType: ProgressType.DONT_KNOW, isBookMarked: false, isAnswredCorrectly: true))
        
        questionList.append(QuestionProgress(id: 7, question: "", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 2, progressType: ProgressType.UN_ANSWRED, isBookMarked: true, isAnswredCorrectly: false))
        
        questionList.append(QuestionProgress(id: 8, question: "", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 3, progressType: ProgressType.UN_ANSWRED, isBookMarked: true, isAnswredCorrectly: false))
        
        questionList.append(QuestionProgress(id: 9, question: "", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 3, progressType: ProgressType.KNOW, isBookMarked: true, isAnswredCorrectly: false))
        
        questionList.append(QuestionProgress(id: 10, question: "", options: ["Anser1","Answer2","Answer3","Answer4"], correctAnswerIndex: 1, imageUrl: nil, explanation: "", sectionId: 3, progressType: ProgressType.UN_ANSWRED, isBookMarked: true, isAnswredCorrectly: false))
        
        inilizeOrUpdateDict()
=======
    public private(set) var inilized:Bool = false
    

    public var lastSynced:Date{get{
            return questionPresistance.lastSynced
        }
    }
    
    public func inilizeOrReferesh()
    {
        questionPresistance = FirebaseQuestionPresistance()
        let questions = informationProvider.questionProvider.getAllQuestions()
        inilized = false
>>>>>>> master_1
        
        
        questionList.removeAll()
        questionDict.removeAll()
        questionPresistance.loadAllQuestions { (dtos) in
            var dict = Dictionary<Int,QuestionProgressDto>()
            
            for dto in dtos
            {
                dict.updateValue(dto, forKey: dto.id)
            }
            
            for q in questions
            {
                let dto = dict[q.id] != nil ? dict[q.id]! : QuestionProgressDto(id: q.id, isBookMarked: false, progressType: 0, isCorrect: false)
                
                let q = QuestionProgress(question: q, progressType:ProgressType(rawValue: dto.progressType)!, isBookMarked: dto.isBookMarked, isAnswredCorrectly: dto.isCorrect)
                q.setSection(section: self.informationProvider.sectionProvider.getSectionForId(id: q.sectionId)!)
                self.questionList.append(q)
            }
            
            self.inilizeOrUpdateDict()
            self.inilized = true
            
            
        }

    }
    
    private func inilizeOrUpdateDict()
    {
        for question in questionList
        {
            questionDict.updateValue(question, forKey: question.id)
        }
    }
    
    public func getQuestionList() -> [QuestionProgress] {
        return questionList
    }
    
    public func getQuestionForId(id: Int) -> QuestionProgress? {
        
        return questionDict[id]
        
    }
    
    public func getQuestionsForIds(idList: [Int]) -> [QuestionProgress?] {
        var questions = [QuestionProgress?]()
        for id in idList
        {
            questions.append(getQuestionForId(id: id))
        }
        return questions
    }
    
    public func updateQuestionProgressType(questionId:Int,progressType:ProgressType)
    {
        //TODO:Need to Add Database Functionality
        getQuestionForId(id: questionId)?.progressType = progressType
        updatePresistance(questionProgres: getQuestionForId(id: questionId)!)
    }
    
    private func updatePresistance(questionProgres:QuestionProgress)
    {
        questionPresistance.updateQuestionProgress(id: questionProgres.id, questionDto: QuestionProgressDto.init(id: questionProgres.id, isBookMarked: questionProgres.isBookMarked, progressType: questionProgres.progressType.rawValue, isCorrect: questionProgres.isAnswredCorrectly), onUpdate: nil)
    }
    
    public func updateBookMarked(questionId:Int,isBookMark:Bool)
    {
        getQuestionForId(id: questionId)?.isBookMarked = isBookMark
        updatePresistance(questionProgres: getQuestionForId(id: questionId)!)
    }
    
    public func updateAnsweredCorrectly(questionId:Int,isAnsweredCorrectly:Bool)
    {
        getQuestionForId(id: questionId)?.isAnswredCorrectly = isAnsweredCorrectly
        updatePresistance(questionProgres: getQuestionForId(id: questionId)!)
    }
}
