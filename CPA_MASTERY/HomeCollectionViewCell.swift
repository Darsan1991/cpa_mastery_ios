//
//  HomeCollectionViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView:UIImageView!
    @IBOutlet weak var label:UILabel!
    @IBOutlet weak var lockView:UIImageView!
    
    func configureCell(tile:HomeTileUI)
    {
        imageView.image = UIImage(named: tile.iconImage)
        label.text = tile.title
        lockView.isHidden = !tile.isPremium
        label.sizeToFit()
    }
}
