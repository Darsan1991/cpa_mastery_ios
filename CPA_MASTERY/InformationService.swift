//
//  InformationService.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class InformationService:InformationProvider
{
    public static var instance = InformationService()
    
    private var questionService:QuestionService!
    private var sectionService:SectionService!
    private var definitionService:DefinitionService!
    private var subjectService:SubjectService!
    
    init() {
        questionService = QuestionService(questions: DBManager.instance.loadQuestions())
        sectionService = SectionService(sections: DBManager.instance.loadSections())
        definitionService = DefinitionService(definitions: DBManager.instance.loadDefinitions())
        subjectService = SubjectService(subjects: DBManager.instance.loadSubjects())
        
        
        questionService.lateReferesh(sectionProvider: sectionService)
        sectionService.lateReferesh(infoProvider: self)
        subjectService.lateReferesh(sectionProvider: sectionService)
        
    }
    
    public var sectionProvider: SectionProvider
    {
        return sectionService
    }
    
    public var subjectProvider: SubjectProvider{
        return subjectService
    }
    
    public var definitionProvider: DefinitionProvider{
        return definitionService
    }
    
    public var questionProvider: QuestionProvider{
        return questionService
    }
    
    public class QuestionService:QuestionProvider
    {
        private var questions:[Question]
        
        
        init(questions:[Question]) {
            self.questions = questions
        }
        
        public func getAllQuestions() -> [Question] {
            return questions
        }
        
        public func getQuestionById(id: Int) -> Question?{
            let index = questions.index { (q) -> Bool in
                return q.id == id
            }
            return index != nil ? questions[index!] : nil
        }
        
        public func lateReferesh(sectionProvider:SectionProvider)
        {
            for question in questions
            {
               question.setSection(section: sectionProvider.getSectionForId(id: question.sectionId)!)
                
            }
        }
        
        
    }
    
    public class SectionService:SectionProvider
    {
        private var sections:[Section]
        
        init(sections:[Section]) {
            self.sections = sections
        }
        
        public func getSectionList() -> [Section] {
            return sections
        }
        
        public func getSectionForId(id: Int) -> Section? {
            let index = sections.index(where: { (section) -> Bool in
                return section.id == id
            })
            
            return  index != nil ? sections[index!] : nil
        }
        
        
        public func lateReferesh(infoProvider:InformationProvider)
        {
            for section in sections
            {
                var questions = [Question]()
                
                for qId in section.relatedQuestionIDs
                {
                    questions.append(infoProvider.questionProvider.getQuestionById(id: qId)!)
                    
                }
                
                section.setRelatedQuestions(relatedQuestions: questions)
                section.setSubject(subject: infoProvider.subjectProvider.getSubjectById(id: section.subjectId)!)
                
            }
        }
        
    }
    
    public class SubjectService:SubjectProvider
    {
        private var subjects:[Subject]
        
        init(subjects:[Subject]) {
            self.subjects = subjects
        }
        
        public func getAllSubjects() -> [Subject] {
            return subjects
        }
        
        public func getSubjectById(id: Int) -> Subject? {
            let index = subjects.index(where: { (subject) -> Bool in
                return subject.id == id
            })
           
            return  index != nil ? subjects[index!] : nil
        }
        
        public func lateReferesh(sectionProvider:SectionProvider)
        {
            for subject in subjects
            {
                var sections = [Section]()
                
               for sec in subject.sectionIds
               {
                 sections.append(sectionProvider.getSectionForId(id: sec)!)

                }
                subject.setSections(sections: sections)
            }
        }
    }
    
    public class DefinitionService:DefinitionProvider
    {
        private var definitions: [Definition]
        
        init(definitions:[Definition]) {
            self.definitions = definitions
        }
        
        public func getDefinitionList() -> [Definition] {
            return definitions
        }
        
        public func getDefinitionForId(id: Int) -> Definition?{
            let index = definitions.index { (definition) -> Bool in
                return definition.id == id
            }
            return index == nil ? nil : definitions[index!]
        }
        
        public func getDefinitionsForIds(idList: [Int]) -> [Definition]? {
            return definitions
        }
    }
}
