//
//  DefinitionPageTileVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/31/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class DefinitionPageTileVC: UIViewController {
    @IBOutlet weak var previousBtn: UIButton!
    @IBOutlet weak var nextBtn: ButtonD!
    
    @IBOutlet weak var titleTxt: UILabel!
    
    @IBOutlet weak var definitionContentTxt: UILabel!

    
    @IBOutlet weak var progressBar: SingleProgressBar!
    
    @IBOutlet weak var definitionIndexLabel: UILabel!
    
    var definitionVM:DefinitionPageTileViewModel!
    
    var delegate:DefinitionPageTileVCDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
    }
    
    func setUp()
    {
//        label.text = definitionVM.definition.title
        titleTxt.text = definitionVM.definition.title
        definitionContentTxt.text = definitionVM.definition.context
        nextBtn.isHidden = definitionVM.index >= definitionVM.maxIndex
        previousBtn.isHidden = definitionVM.index <= 0
        progressBar.progress = CGFloat(100*(definitionVM.index + 1)/(definitionVM.maxIndex+1))
        definitionIndexLabel.text = "\(definitionVM.index+1) of \(definitionVM.maxIndex+1)"
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onClickNext(_ sender: Any) {
        delegate?.definitionPageTile(click_next_button_: self)
    }
    
    @IBAction func onClickPrevious(_ sender: Any) {
        delegate?.definitionPageTile(click_previous_button_: self)
    }
    

    public class DefinitionPageTileViewModel
    {
        var definition:Definition
        var index:Int
        var maxIndex:Int
        
        init(definition:Definition,index:Int,maxIndex:Int) {
            self.definition = definition
            self.index = index
            self.maxIndex = maxIndex
        }
    }

}


