//
//  QuizAndAnswerParentVCDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/2/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation


public protocol QuizAndAnswerParentVCDelegate
{
    func quizAndAnswerParentVC(clickedNextButton vc:UIViewController)->Void
    func quizAndAnswerParentVC(clickedPreviousButton vc:UIViewController)->Void
    func quizAndAnswerParentVC(progress:ProgressType, selectedProgress vc:UIViewController)->Void
    func quizAndAnswerParentVC(bookMark isBookMark:Bool,questionProgress:QuestionProgress, vc:UIViewController)->Void
}
