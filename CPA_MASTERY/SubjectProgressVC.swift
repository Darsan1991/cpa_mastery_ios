//
//  SubjectProgressVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/23/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class SubjectProgressVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SelectionFilterViewCellDelegate {
    
    
    
    @IBOutlet weak var tableView:UITableView!
    
    private var premiumHeaderSize = CGFloat(25)
    
    var subject : Subject!
    private var selectionFilterData:SelectionFilterData!
    private var avalivableViewModels = [SubjectProgressViewModel]()
    private var premiumViewModels = [SubjectProgressViewModel]()
    private var currentFiler:Filter = Filter.BOOKMARK
    private var sectionProvider = InformationService.instance.sectionProvider
    private var questionProvider:QuestionProgressProvider = QuestionProgressService.instance
    private var globalService:GlobalService = GlobalService.instance
    
    
    private var leavedView:Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
       
        inilizeSectionFilter()
        currentFiler = Filter.UN_ANSWRED
        
       inilizeViewModels()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        onFilterButtonClicked(filter: currentFiler)
        
        
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
//        print("Reveal View Controller:\(revealViewController())")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if leavedView
        {
            leavedView = false
            refershViewModels()
            tableView.reloadData()
        }
    }
    
    
    
    private func refershViewModels()
    {
        selectionFilterData.bookMarkProgressAdapter.referesh()
        selectionFilterData.questionProgressAdapter.referesh()
       for vm in avalivableViewModels
       {
            vm.questionAdapter.referesh()
            vm.bookMarkAdapter.referesh()
        }
        
        for vm in premiumViewModels
        {
            vm.questionAdapter.referesh()
            vm.bookMarkAdapter.referesh()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        leavedView = true
        print("Reveal View Controller:\(revealViewController())")
    }
    
    private func inilizeSectionFilter()
    {
        var questionList = [QuestionProgress]()
        
        for sec in subject.sections!
        {
            questionList.append(contentsOf: questionProvider.getQuestionsForIds(idList: sec.relatedQuestionIDs) as! [QuestionProgress])
        }
        
         selectionFilterData = SelectionFilterData(questionProgresses:questionList)
    }

    private func inilizeViewModels()    {
        //var sectionIds =
        let sections = subject.sections!
        var freeList = [Section]()
        var premiumList = [Section]()
        
        
        
        for section in sections
        {
            if section.isPremium
            {
                premiumList.append(section)
            }
            else
            {
                freeList.append(section)
            }
        }
        
        
        for sec in freeList
        {
             let questions =  questionProvider.getQuestionsForIds(idList:sec.relatedQuestionIDs)
            avalivableViewModels.append( SubjectProgressViewModel(section: sec, questionAdapter: QuestionProgressAdapter(totalQuestions: questions as! [QuestionProgress]), bookMarkAdapater: BookMarkProgressAdapter(totalQuestions: questions as! [QuestionProgress]), isLocked: false, filter: Filter.UN_ANSWRED))
        }
        for sec in premiumList
        {
            let questions =  questionProvider.getQuestionsForIds(idList:sec.relatedQuestionIDs)

            premiumViewModels.append( SubjectProgressViewModel(section: sec, questionAdapter: QuestionProgressAdapter(totalQuestions: questions as! [QuestionProgress]), bookMarkAdapater: BookMarkProgressAdapter(totalQuestions: questions as! [QuestionProgress]), isLocked: true && !globalService.isPremium, filter: Filter.UN_ANSWRED))
        }
        
        if globalService.isPremium
        {
            avalivableViewModels.append(contentsOf: premiumViewModels)
            premiumViewModels.removeAll()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section==0
        {
        return 1
        }
        else if section == 1{
            return avalivableViewModels.count
        }
        else if section == 2{
            return premiumViewModels.count
        }
        return 0
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 2
        {
            return premiumHeaderSize
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0
        {
            return 144
        }
        else{
            return 80
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 2 && premiumViewModels.count > 0
        {
            let textWidth = CGFloat(100)
            let lineHeigt = CGFloat(1)
            let headerHeight = premiumHeaderSize
            let textColor = UIColor.darkGray
            let lineColor = UIColor.gray
            let bgColor = UIColor.lightGray
            
            let header = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: headerHeight))
            header.backgroundColor = bgColor
            
            let text = UILabel(frame: CGRect(x: view.frame.width/2 - textWidth/2, y: 0, width: textWidth, height: headerHeight))
            text.text = "PREMIUM"
            text.textAlignment = .center
            text.backgroundColor = UIColor.clear
            text.textColor = textColor
            text.font = UIFont(name: text.font.fontName, size: 13)
            header.addSubview(text)
            
            
            let leftLine = UIView(frame:CGRect(x:0,y:(headerHeight - lineHeigt)/2,width:(view.frame.width - textWidth)/2,height:lineHeigt))
            leftLine.backgroundColor = lineColor
            
            let rightLine = UIView(frame:CGRect(x:(view.frame.width + textWidth)/2,y:(headerHeight - lineHeigt)/2,width:(view.frame.width - textWidth)/2,height:lineHeigt))
            rightLine.backgroundColor = lineColor
            
            header.addSubview(leftLine)
            header.addSubview(rightLine)
            
            return header
        }
        return nil
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1
        {
            if avalivableViewModels[indexPath.row].getActiveQuestions().count == 0
            {
                return
            }
            openQuizVC(questions: avalivableViewModels[indexPath.row].getActiveQuestions())
        }
        else if indexPath.section == 2
        {
            PopUpHandler(vc: self).show()
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0
        {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "SelectionFilterCell") as? SelectionFilterViewCell
            {
                cell.delegate = self
                cell.selectedFilter = currentFiler
                return cell
            }
        }
        else if indexPath.section == 1 || indexPath.section == 2
        {
           if let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectProgressViewCell") as? SubjectProgressViewCell
           {
            cell.configureCell(cellModel: indexPath.section == 1 ? avalivableViewModels[indexPath.row] : premiumViewModels[indexPath.row])
                return cell
            }
        }
        
        return UITableViewCell()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return premiumViewModels.count > 0 ? 3 : 2
    }
    
    
    
    //Selection Filter
    func onFilterButtonClicked(filter: Filter) {
        if currentFiler == filter
        {
            return
        }
        currentFiler = filter
        for model in avalivableViewModels
        {
            model.currentFilter = currentFiler
        }
        
        for model in premiumViewModels
        {
            model.currentFilter = currentFiler
        }
        
        NotificationCenter.default.post(name: FILTER_UPDATED_EVENT, object: nil)
    }
    
    func onQuickStartClicked(questionProgresses: [QuestionProgress]) {
       openQuizVC(questions: questionProgresses)
    }
    
    private func openQuizVC(questions:[QuestionProgress])
    {
         performSegue(withIdentifier: QUIZ_GROUP_VC_SEGUE, sender: questions)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == QUIZ_GROUP_VC_SEGUE
        {
            if let ques = sender as? [QuestionProgress]
            {
                (segue.destination as! QuizGroupVC).questions  = ques
            }
        }
    }
    
    func filterAdapters()->(QuestionProgressAdapter,BookMarkProgressAdapter)
    {
        return (selectionFilterData.questionProgressAdapter,selectionFilterData.bookMarkProgressAdapter)
    }
    
    
    
    
    private class SelectionFilterData
    {
        var questionProgressAdapter:QuestionProgressAdapter!
        var bookMarkProgressAdapter:BookMarkProgressAdapter!
        
        init(questionProgresses:[QuestionProgress]) {
            questionProgressAdapter = QuestionProgressAdapter(totalQuestions: questionProgresses)
            bookMarkProgressAdapter = BookMarkProgressAdapter(totalQuestions: questionProgresses)
        }
    }

}
