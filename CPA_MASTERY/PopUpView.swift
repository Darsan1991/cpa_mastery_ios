//
//  PopUpView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/30/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

public class PopUpView: UIView ,UIGestureRecognizerDelegate{
    
    @IBOutlet weak var contentView:UIView!
    var delegate:PopUpViewDelegate?

    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }

    func initSubviews() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = bounds
        addSubview(contentView)
        setUpDismissAtClick()
        
        
    }
    
    func setUpDismissAtClick()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(removeFromSuperview))
        tap.cancelsTouchesInView = false
        tap.delegate = self
        self.addGestureRecognizer(tap)
    }
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
    
        let recieve = !contentView.bounds.contains(touch.location(in: contentView))
       print("recieve  \(recieve) frame:\(contentView.frame) location:\(touch.location(in: contentView))" )
        return recieve
        
    }

    
    @IBAction func onPositiveButton(_ sender: Any) {
        delegate?.onPositiveButtonClicked()
    }
    
    @IBAction func onNegativeButton(_ sender: Any) {
        delegate?.onNegativeButtonClicked()
    }
}
