//
//  ResetUserPasswordVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/24/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ResetUserPasswordVC: UIViewController{
    
    @IBOutlet weak var resetBtn: ButtonD!
    @IBOutlet weak var enterNewPassword: TextFieldD!
    
    @IBOutlet weak var confirmPassword: TextFieldD!

    private var accountHandler = GlobalService.instance.accountHandler
    
    override func viewDidLoad() {
        super.viewDidLoad()

         enterNewPassword.addTarget(self, action: #selector(textDidChanged), for: UIControlEvents.editingChanged)
        confirmPassword.addTarget(self, action: #selector(textDidChanged), for: UIControlEvents.editingChanged)
        
    }
    
    @IBAction func onClickResetPassword(_ sender: Any) {
        let progress = ProgressHandler()
        progress.show()
        accountHandler?.updatePassword(newPassword: enterNewPassword.text!, taskFinished: { (sucess) in
            progress.dismiss()
            self.dismiss(animated: true, completion: nil)
        })
        
    }
    
    public func textDidChanged()
    {
      resetBtn.isUserInteractionEnabled = (enterNewPassword.text != nil && enterNewPassword.text!.characters.count > 0 && enterNewPassword.text == confirmPassword.text)
    }
    
    

}
