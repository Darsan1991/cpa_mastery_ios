//
//  Progress.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/23/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class Progress: UIView {
    
    @IBOutlet weak var activityIndicatorContainer: UIView!
    
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()


    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initSubviews()
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initSubviews()
    }


    
    func initSubviews() {
        // standard initialization logic
        //        let bundle = Bundle(for: type(of: self))
        //        let nib = UINib(nibName: "SelectionFilterGroupView", bundle: bundle)
        //        let contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = bounds
        addSubview(contentView)
        
        print("width\(activityIndicatorContainer.frame.width)")
        activityIndicator.frame = CGRect(x: 0, y: 0, width: activityIndicatorContainer.frame.width, height: activityIndicatorContainer.frame.height)
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        activityIndicator.center = CGPoint(x: activityIndicatorContainer.frame.size.width / 2, y: activityIndicatorContainer.frame.size.height / 2)
        activityIndicatorContainer.addSubview(activityIndicator)
        activityIndicator.startAnimating()
    }
}
