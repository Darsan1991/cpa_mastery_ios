//
//  MultiProgressBar.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/19/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class MultiProgressBar: UIView {
    
    
    let barBackgroundColor = UIColor(red: CGFloat(235)/255, green: CGFloat(237)/255, blue: CGFloat(239)/255, alpha: 1)
    let dontKnowColor = UIColor(red: CGFloat(205)/255, green: CGFloat(102)/255, blue: CGFloat(100)/255, alpha: 1)
    let someHowKnowColor = UIColor(red: CGFloat(255)/255, green: CGFloat(211)/255, blue: CGFloat(125)/255, alpha: 1)
    let knowColor = UIColor(red: CGFloat(123)/255, green: CGFloat(213)/255, blue: CGFloat(110)/255, alpha: 1)
    
    var dontKnowView:UIView!
    var someHowKnowView:UIView!
    var knowView:UIView!
    var backgroundView:UIView!
    
    var dontKnowWidthConstraint:NSLayoutConstraint!
    var someHowKnowConstraint:NSLayoutConstraint!
    var knowthConstraint:NSLayoutConstraint!
    
    private var adapter:ProgressAdapter!
    private var isInilize:Bool = false

    override func awakeFromNib() {
        super.awakeFromNib()
        //background view
        if isInilize
        {
            return
        }
         backgroundView = UIView()
        backgroundView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: frame.height)

        backgroundView.center = CGPoint(x: frame.width/2, y: frame.height/2)
        backgroundView.clipsToBounds = true
        backgroundView.backgroundColor = barBackgroundColor
        self.addSubview(backgroundView)
        }
    
    func setAdapterAndShowAllProgress(adapter:ProgressAdapter,callback:Callback?=nil)
    {
        if self.adapter !== adapter
        {
            self.adapter = adapter
            adapter.addDatachangedListner(onDataChanged: adapterDataChanged)
        }
        
       
       
        if !isInilize
        {
            
            DispatchQueue.global(qos: .background).async {
                print("This is run on the background queue")

                
                DispatchQueue.main.asyncAfter(deadline: .now()+0.1) {
                    
                    self.setUpBar()
                    self.showAllView()
                    if let c = callback
                    {
                        c()
                    }
                }
            }
        }
        else
        {
            setUpBar()
            showAllView()
            if let c = callback
            {
                c()
            }

        }
    }
    
    func adapterDataChanged()
    {
        setUpBar()
    }
    

    func setUpBar()
    {
         let totalCount = adapter.getTotalQuestionCount()
        let dontKnowWidth = self.frame.width * CGFloat(adapter.getQuestionCount(progressType: ProgressType.DONT_KNOW))/CGFloat(totalCount)
        let someHowKnowWidth = self.frame.width * CGFloat(adapter.getQuestionCount(progressType: ProgressType.SOME_HOW_KNOW))/CGFloat(totalCount)
        let knowWidth = self.frame.width * CGFloat(adapter.getQuestionCount(progressType: ProgressType.KNOW))/CGFloat(totalCount)
        
        self.setUpBarWidths(
            dontKnowWidth:dontKnowWidth
            , someHowKnowWidth:someHowKnowWidth
            , knowWidth:knowWidth)
    }
    
    func setAdapterAndShowOneProgress(adapter:ProgressAdapter,type:ProgressType)
    {
        setAdapterAndShowAllProgress(adapter: adapter){
            if type == ProgressType.UN_ANSWRED
            {
                return
            }
            self.showOnlyOneView(view: self.getViewForType(type: type))
        }
       
    }
    
    func showOnlyOneProgress(type:ProgressType)
    {
        if !isInilize
        {
            return
        }
        showOnlyOneView(view: getViewForType(type: type))
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if isInilize
        {
            return
        }
        
        backgroundView.frame = CGRect(x: 0, y: 0, width: frame.width, height: backgroundView.frame.height)
        backgroundView.layer.cornerRadius = frame.height/2
        backgroundView.layoutIfNeeded()
        
        //stack View
        let stackView = UIStackView()
        stackView.frame = CGRect(x: 0, y: 0, width: backgroundView.frame.width, height: backgroundView.frame.height)
        stackView.axis = .horizontal
        stackView.autoresizesSubviews = false
        
        backgroundView.addSubview(stackView)
        
        
        
        var out = createBarView(color: dontKnowColor)
        dontKnowView = out.view
        dontKnowWidthConstraint = out.constraint
        stackView.addArrangedSubview(dontKnowView)
        
        out = createBarView(color: someHowKnowColor)
        someHowKnowView = out.view
        someHowKnowConstraint = out.constraint
        stackView.addArrangedSubview(someHowKnowView)
        
        out = createBarView(color: knowColor)
        knowView = out.view
        knowthConstraint = out.constraint
        stackView.addArrangedSubview(knowView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        isInilize = true

//        setAdapterAndShowOneProgress(adapter: adapter, type: ProgressType.DONT_KNOW)
//        _ = Timer.scheduledTimer(withTimeInterval: 5, repeats: false, block: { (time) in
//            self.setAdapterAndShowOneProgress(adapter: self.adapter, type: ProgressType.SOME_HOW_KNOW)
//        })
//
//        let totalCount = adapter.getTotalQuestionCount()
//        setUpBarWidths(
//            dontKnowWidth: frame.width * CGFloat(adapter.getQuestionCount(progressType: ProgressType.DONT_KNOW)/totalCount)
//            , someHowKnowWidth: frame.width * CGFloat(adapter.getQuestionCount(progressType: ProgressType.SOME_HOW_KNOW)/totalCount)
//            , knowWidth: frame.width * CGFloat(adapter.getQuestionCount(progressType: ProgressType.KNOW)/totalCount)
//        )
        
//        showOnlyOneView(view: knowView)
 
    }
        
    private func setUpBarWidths(dontKnowWidth:CGFloat,someHowKnowWidth:CGFloat,knowWidth:CGFloat)
    {
        if !isInilize
        {
            return
        }
        dontKnowWidthConstraint.constant = dontKnowWidth
        dontKnowView.layoutIfNeeded()
        someHowKnowConstraint.constant = someHowKnowWidth
        someHowKnowView.layoutIfNeeded()
        knowthConstraint.constant = knowWidth
        knowView.layoutIfNeeded()
    }
    
    private func showAllView()
    {
        dontKnowView.isHidden = false
        someHowKnowView.isHidden = false
        knowView.isHidden = false
    }
    
    private func showOnlyOneView(view:UIView)
    {
        dontKnowView.isHidden = dontKnowView != view
        knowView.isHidden = knowView != view
        someHowKnowView.isHidden = someHowKnowView != view
        setNeedsDisplay()
        needsUpdateConstraints()
        setNeedsLayout()
        
    }
    
    private func createBarView(color:UIColor) -> (view:UIView,constraint:NSLayoutConstraint)
    {
        let view  = UIView()
        let outwidthConstraint =   view.widthAnchor.constraint(equalToConstant: 0)
        outwidthConstraint.isActive = true
        view.heightAnchor.constraint(equalToConstant: backgroundView.frame.height).isActive = true
        view.backgroundColor = color
        
        return (view,outwidthConstraint)
    }
    
    private func getViewForType(type:ProgressType) -> UIView
    {
        switch type {
        case .DONT_KNOW:
            return dontKnowView
            
        case .KNOW:
            return knowView
            
        case .SOME_HOW_KNOW:
            return someHowKnowView
            
        default:
            return someHowKnowView
        }
    }

    
}
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */


