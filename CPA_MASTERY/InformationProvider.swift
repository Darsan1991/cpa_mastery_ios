//
//  InformationProvider.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol InformationProvider{
    var definitionProvider:DefinitionProvider{get}
    var questionProvider:QuestionProvider{get}
    var sectionProvider:SectionProvider{get}
    var subjectProvider:SubjectProvider{get}
}

public protocol DefinitionProvider
{
    func getDefinitionsForIds(idList:[Int])->[Definition]?
    func getDefinitionForId(id:Int)->Definition?
    func getDefinitionList()->[Definition]
}

public protocol QuestionProvider
{
    func getQuestionById(id:Int)->Question?
    func getAllQuestions() -> [Question]
}

public protocol SectionProvider
{
   func getSectionList()->[Section]
   func getSectionForId(id:Int)->Section?
}

public protocol SubjectProvider
{
    func getSubjectById(id:Int)->Subject?
    func getAllSubjects()->[Subject]
}


