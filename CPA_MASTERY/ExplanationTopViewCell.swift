//
//  ExplanationTopViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/4/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ExplanationTopViewCell: UITableViewCell {
    
    private static var rightImageName = "ic_check_circle_white"
    private static var wrongImageName = "ic_highlight_off_white"
    private static var rightColor:UIColor = UIColor.green
    private static var wrongcolor:UIColor = UIColor.red

    @IBOutlet weak var segment:UISegmentedControl!
    @IBOutlet weak var topBar:UIView!
    @IBOutlet weak var topBarTitle:UILabel!
    @IBOutlet weak var topBarIcon:UIImageView!
    @IBOutlet  var q1View:UIView!
    @IBOutlet  var e2View:UIView!
    
    @IBOutlet weak var explanationLabel:UILabel!
    @IBOutlet weak var questionLabel:UILabel!
    @IBOutlet weak var progressBar:MultiProgressBar!
    @IBOutlet weak var sectionTitleLabel:UILabel!
    @IBOutlet weak var indexLabel:UILabel!
    
    @IBOutlet var qView:ExplanationTopQuizView!
    @IBOutlet var eView:ExplanationTopAnswerView!
    
    var delegate:ExplanationTopViewCellDelegate?
    
    private var explanitionView:UIView?
    private var questionView:UIView?
    
    private var qContraints:[NSLayoutConstraint]!
    private var eContraints:[NSLayoutConstraint]!
    
    var viewModel:ExplanationTopViewCellModel
    {
        set(newVal)
        {
            _viewModel = newVal
            configureCell(vm: _viewModel)
        }
        get{
            return _viewModel
        }
    }
    
    private var _viewModel:ExplanationTopViewCellModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        eContraints = e2View.constraints
        qContraints = q1View.constraints
        segment.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
        // Initialization code
    }

    func getHeight(isExplantion:Bool)->CGFloat
    {
        let height = isExplantion ? e2View.frame.height : q1View.frame.height
        return height
    }
    
    private func configureCell(vm:ExplanationTopViewCellModel)
    {
        //set up top bar
        topBar.backgroundColor = vm.state == State.RIGHT ? ExplanationTopViewCell.rightColor : ExplanationTopViewCell.wrongcolor
        topBarIcon.image = UIImage(named: vm.state == State.RIGHT ? ExplanationTopViewCell.rightImageName : ExplanationTopViewCell.wrongImageName)
        topBarTitle.text = vm.state == State.RIGHT ? "Correct" : "Wrong"
        
        segment.selectedSegmentIndex = vm.isExplanation ? 0 : 1
        
        e2View.isHidden = segment.selectedSegmentIndex != 0
        q1View.isHidden = segment.selectedSegmentIndex != 1
         explanationLabel.text = "This is Explanation This is Explanation This is Explanation This is Explanation this is explanation this is Exlplanation This is Explanation This is Explanation This is Explanation This is Explanation this is explanation this is Exlplanation This is Explanation This is Explanation This is Explanation This is Explanation this is explanation this is Exlplanation This is Explanation This is Explanation This is Explanation This is Explanation this is explanation this is Exlplanation"
        //set up explanation
        if segment.selectedSegmentIndex == 0
        {
            
//            if e2View.superview == nil
//            {
//                addSubview(e2View)
//                e2View.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor).isActive = true
//                e2View.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor).isActive = true
//                
//                e2View.leftAnchor.constraint(equalTo: layoutMarginsGuide.leftAnchor).isActive = true
//                e2View.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor).isActive = true
//            }
//            q1View.removeFromSuperview()
            
           //vm.question.explanation
//            explanitionView  = explanitionView == nil ? ExplanationTopAnswerView() : explanitionView
//
//            if explanitionView?.superview == nil
//            {
//                
//            addSubview(explanitionView!)
//                explanitionView?.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor).isActive = true
//                explanitionView?.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor).isActive = true
//                
//                explanitionView?.leftAnchor.constraint(equalTo: layoutMarginsGuide.leftAnchor).isActive = true
//                explanitionView?.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor).isActive = true
//            }
//            explanationLabel.text = vm.question.explanation
        }
        else if segment.selectedSegmentIndex == 1
        {
//            if q1View.superview == nil
//            {
//                addSubview(q1View)
//            }
//            
          
        
//            questionView  = questionView == nil ? ExplanationTopQuizView(frame: CGRect(x:0,y:60,width:frame.width,height:120), vm: ExplanationTopQuizView.ExplanationTopQuizViewModel(
//                indicatorText: "\(vm.index) of \(vm.questionAdapter.totalQuestionList.count)"
//                , sectionText: vm.question.section?.title
//                , questionText: vm.question.question
//                , progressBar: vm.questionAdapter
//            )) : questionView
//            
//            if questionView?.superview == nil
//            {
//                 addSubview(questionView!)
//            }
           
           // addSubview(explanationView)
        //set up question
//        questionLabel.text = vm.question.question
//        progressBar.setAdapterAndShowAllProgress(adapter: vm.questionAdapter)
//        sectionTitleLabel.text = vm.question.section?.title
//        indexLabel.text = "\(vm.index) of \(vm.questionAdapter.totalQuestionList.count)"
        }

    }

    func segmentedControlValueChanged(segment: UISegmentedControl) {
       
        e2View.isHidden = segment.selectedSegmentIndex != 0
        q1View.isHidden = segment.selectedSegmentIndex != 1
//        if segment.selectedSegmentIndex == 0
//        {
//            print("E2 Frame:\(e2View.frame)")
//            if e2View.superview == nil
//            {
//                
//                                e2View.removeConstraints(eContraints)
//                //                e2View.addConstraints(eContraints)
//                 e2View.frame = CGRect(x: 0, y: 150, width: frame.width, height: 120)
//                addSubview(e2View)
//               
////                e2View.removeConstraints(eContraints)
////                e2View.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor,constant:50).isActive = true
////                                e2View.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor).isActive = true
////                
////                                e2View.leftAnchor.constraint(equalTo: layoutMarginsGuide.leftAnchor).isActive = true
////                                e2View.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor).isActive = true
////                
////                eContraints = e2View.constraints
////                layoutIfNeeded()
////                setNeedsDisplay()
////                needsUpdateConstraints()
////                setNeedsLayout()
////                setNeedsUpdateConstraints()
////                
//                e2View.layoutIfNeeded()
////                e2View.needsUpdateConstraints()
//                e2View.setNeedsDisplay()
////                e2View.setNeedsLayout()
////                e2View.setNeedsUpdateConstraints()
//                print("E2 Frame:\(e2View.frame)")
//            }
//            q1View?.removeFromSuperview()
//
//        }
//        else if segment.selectedSegmentIndex == 1
//        {
//            e2View?.removeFromSuperview()
//            if q1View.superview == nil
//            {
//                addSubview(q1View)
//                
//                q1View.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor,constant:50).isActive = true
//                q1View.bottomAnchor.constraint(equalTo: layoutMarginsGuide.bottomAnchor).isActive = true
//                
//                q1View.leftAnchor.constraint(equalTo: layoutMarginsGuide.leftAnchor).isActive = true
//                q1View.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor).isActive = true
//                layoutIfNeeded()
//                setNeedsLayout()
//                setNeedsDisplay()
//            }
//            
//        }
//        
        
        delegate?.explanationTopViewCell(changed_segment: segment.selectedSegmentIndex)
    
    }
    
    
    public enum State
    {
        case RIGHT
        case WRONG
    }
}


 class ExplanationTopViewCellModel
{
    var state:ExplanationTopViewCell.State
    var question:QuestionProgress
    var questionAdapter:QuestionProgressAdapter
    var index:Int
    var isExplanation:Bool
    
    init(state:ExplanationTopViewCell.State,question:QuestionProgress,index:Int,questionProgressAdapter:QuestionProgressAdapter,isExplanation:Bool) {
        self.state = state
        self.questionAdapter = questionProgressAdapter
        self.question = question
        self.index = index
        self.isExplanation = isExplanation
    }
}
