//
//  QuizAndAnswerParentVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/2/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class QuizAndAnswerParentVC: UIViewController,QuizVCDelegate,ExplanationVCDelegate,QuizNavagationDrawerVCDelegate{

    @IBOutlet weak var quizContainer: UIView!
    
    @IBOutlet weak var explanationContainer: UIView!
    var quizVC:QuizVC!
    var explanationVC:ExplanationVC!
    
    var delegate:QuizAndAnswerParentVCDelegate?
    
    var viewModel:QuizAndAnswerParentVCViewModel!
    
    private var quizNavagationDrawer:QuizNavagationDrawerVC?
    
    override func viewDidLoad() {
        super.viewDidLoad()

       
        initContainers()
       
//        configureRevalController()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        quizNavagationDrawer = QuizNavagationDrawerVC.instance
        quizNavagationDrawer?.delegate = self
        quizNavagationDrawer?.questionProgress = viewModel.questionProgress
    }
    
    
    func quizNavagationDrawer(onBackToCategoryClick vc: UIViewController) {
        navigationController?.popViewController(animated: true)
    }
    
    func quizNavagationDrawer(onClickBookMark bookMark: Bool, vc: UIViewController) {
        delegate?.quizAndAnswerParentVC(bookMark: bookMark, questionProgress: viewModel.questionProgress, vc: vc)
    }
    
   

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "QuizVC"
        {
            quizVC = segue.destination as! QuizVC
            quizVC.delegate = self
            quizVC.viewModel = QuizVC.QuizVCViewModel(question: viewModel.questionProgress,questionAdapter:viewModel.questionProgressAdapter, index: viewModel.index, maxIndex: viewModel.maxIndex)
        }
        else if segue.identifier == "ExplanationVC"
        {
            explanationVC = segue.destination as! ExplanationVC
           
            explanationVC.delegate = self
        }
        
    }
    
    
    private func moveToExplanationVC()
    {
        UIView.animate(withDuration: 0.3, delay: 0, options: .allowAnimatedContent, animations: {
            self.quizContainer.layer.transform = CATransform3DScale(self.quizContainer.layer.transform, 0.93, 0.93, 1)
            self.quizContainer.layer.transform = CATransform3DRotate(self.quizContainer.layer.transform, -CGFloat(M_PI)*0.5, 0, 1, 0)
            self.quizContainer.alpha = 0.5
            
        }) { (sucess) in
            self.explanationContainer.isHidden = true
            self.explanationContainer.isHidden = false
            self.explanationContainer.alpha = 0.3
            let transform = CATransform3DScale(self.explanationContainer.layer.transform, 0.93, 0.93, 0.93)
            self.explanationContainer.layer.transform = CATransform3DRotate(transform, 0.5*CGFloat(M_PI), 0, 1, 0)
            
            
            
            UIView.animate(withDuration: 0.3, delay: 0, options: .allowAnimatedContent, animations: {
                
                let targetScale = CGFloat(1/0.93)
                let transform = CATransform3DScale(self.explanationContainer.layer.transform, targetScale, targetScale,targetScale)
                self.explanationContainer.layer.transform = CATransform3DRotate(transform, -CGFloat(M_PI)*0.5, 0, 1, 0)
                self.explanationContainer.alpha = 1
                
                
                
            },completion: nil)
        }

    }
    
    private func initContainers()
    {
        explanationContainer.isHidden = true
    }
    
    func quizVc(nextButton vc: UIViewController) {
        delegate?.quizAndAnswerParentVC(clickedNextButton: self)
    }

    func quizVc(previousButton vc: UIViewController) {
         delegate?.quizAndAnswerParentVC(clickedPreviousButton: self)
        
    }
    
    func quizVc(selectedAnswer: Int, submitButton vc: UIViewController) {
<<<<<<< HEAD
        
         explanationVC.explanationVM = ExplanationVC.ExplanationVCViewModel(question: viewModel.questionProgress, questionAdapter: viewModel.questionProgressAdapter, selectedAnswer: selectedAnswer, index: viewModel.index)
//        explanationVC.explanationVM = ExplanationVC.ExplanationVCViewModel(question: (vc as! QuizVC).question, selectedAnswer: selectedAnswer)
=======
        explanationVC.explanationVM = ExplanationVC.ExplanationVCViewModel(question: viewModel.questionProgress, questionAdapter: viewModel.questionProgressAdapter, selectedAnswer: selectedAnswer, index: viewModel.index)
>>>>>>> master_1
        moveToExplanationVC()
    }
    
    func explanationVC(progressType: ProgressType, selectProgress vc: UIViewController) {
        delegate?.quizAndAnswerParentVC(progress: progressType, selectedProgress: self)
    }
    
    public class QuizAndAnswerParentVCViewModel{
        var questionProgressAdapter:QuestionProgressAdapter
        var questionProgress:QuestionProgress
        var index:Int
        var maxIndex:Int
        
        init(questionProgress:QuestionProgress,questionProgressAdapter:QuestionProgressAdapter,index:Int,maxIndex:Int) {
            self.questionProgress = questionProgress
            self.questionProgressAdapter = questionProgressAdapter
            self.index = index
            self.maxIndex = maxIndex
            self.questionProgressAdapter = questionProgressAdapter
        }
    }
    
}
