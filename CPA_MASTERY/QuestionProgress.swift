//
//  QuestionProgress.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class QuestionProgress:Question
{
    public  var progressType:ProgressType
    public  var isBookMarked:Bool
    public  var isAnswredCorrectly:Bool
    
    init(id: Int, question: String, options: [String], correctAnswerIndex: Int, imageUrl: String?, explanation: String, sectionId: Int,progressType:ProgressType,isBookMarked:Bool,isAnswredCorrectly:Bool) {
        self.progressType = progressType
        self.isBookMarked = isBookMarked
        self.isAnswredCorrectly = isAnswredCorrectly
        super.init(id: id, question: question, options: options, correctAnswerIndex: correctAnswerIndex, imageUrl: imageUrl, explanation: explanation, sectionId: sectionId)
    }
    
    init(question:Question,progressType:ProgressType,isBookMarked:Bool,isAnswredCorrectly:Bool)
    {
        self.progressType = progressType
        self.isBookMarked = isBookMarked
        self.isAnswredCorrectly = isAnswredCorrectly
         super.init(id: question.id, question: question.question, options: question.options, correctAnswerIndex: question.correctAnswerIndex, imageUrl: question.imageUrl, explanation: question.explanation, sectionId: question.sectionId)
        
        
    }
    
}
