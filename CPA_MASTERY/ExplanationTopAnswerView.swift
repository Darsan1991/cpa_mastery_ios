//
//  ExplanationTopAnswerView.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/4/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ExplanationTopAnswerView: UIView {
    @IBOutlet weak var explanationLabel: UILabel!
    
    var explanation:String?
    
    override func awakeFromNib() {
        explanationLabel.text = explanation
    }

    func initSubviews() {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let contentView = nib.instantiate(withOwner: self, options: nil).first as! UIView
        contentView.frame = bounds
        addSubview(contentView)
    }
    
    override init(frame: CGRect) {
        super.init(frame:frame)
        initSubviews()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder:aDecoder)
        initSubviews()
    }
    
    init(frame:CGRect,explanation:String) {
        super.init(frame:frame)
        initSubviews()
        self.explanation = explanation
    }


}
