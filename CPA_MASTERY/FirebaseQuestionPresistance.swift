//
//  FirebaseQuestionPresistance.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation
import FirebaseDatabase
import FirebaseAuth

public class FirebaseQuestionPresistance:QuestionProgressPresistance
{
    public  static let idPrefix = "question_";
    public var onSync: QuestionProgressPresistance.OnSync?
    
    private var ref:DatabaseReference
    
    public var lastSynced: Date = Date()
    
    init() {
        ref = Database.database().reference().child("users").child(Auth.auth().currentUser!.uid).child("QuestionProgresses")
        
    }
    
    public func reset(onTaskFinished: OnTaskFinished?) {
        
        ref.removeValue { (err, ref) in
            onTaskFinished?(err == nil)
            if err == nil{
                
               self.synced()
            }
        }

    }
    
    private func synced()
    {
        lastSynced = Date()
        onSync?()
    }
    
    public func loadAllQuestions(onLoadAll: QuestionProgressPresistance.OnLoadAll?) {
        ref.keepSynced(true)
        ref.observeSingleEvent(of: .value, with: { (snapShot) in
            var questionProgressDtos = [QuestionProgressDto]()
            
            if(snapShot.childrenCount > 0)
            {
                print("Path:\(snapShot.key)")
                print("Childeren Count:\(snapShot.childrenCount)")
               for child in snapShot.children
               {
                print("Child:\(child)")
                    let dict = (child as! DataSnapshot).value as? [String:AnyObject] ?? [:]
                    let id = dict["id"] as! Int
                    let progress = dict["progressType"] as! Int
                    let bookMarked = dict["isBookMarked"] as! Bool
                    let correct = dict["isCorrect"] as! Bool
                
                    questionProgressDtos.append(QuestionProgressDto(id: id, isBookMarked: bookMarked, progressType: progress, isCorrect: correct))
                }
            }
            
            
           
            self.ref.keepSynced(false);
            onLoadAll?(questionProgressDtos)
            
            self.synced()
        })
    }
    
    public func loadQuestionProgress(id: Int, onLoadQuestion: QuestionProgressPresistance.OnLoad?) {
        ref.child("\(FirebaseQuestionPresistance.idPrefix)\(id)").observeSingleEvent(of: .value, with: { (snapShot) in
            
                let dict = snapShot.value as? [String:AnyObject] ?? [:]
                let id = dict["id"] as! Int
                let progress = dict["progressType"] as! Int
                let bookMarked = dict["isBookMarked"] as! Bool
                let correct = dict["isCorrect"] as! Bool
                
                let dto = QuestionProgressDto(id: id, isBookMarked: bookMarked, progressType: progress, isCorrect: correct)
            
            onLoadQuestion?(dto)

            self.synced()
        })
    }
    
    public func updateQuestionProgress(id: Int, questionDto: QuestionProgressDto, onUpdate: QuestionProgressPresistance.OnUpdate?) {
        
        ref.child("\(FirebaseQuestionPresistance.idPrefix)\(id)").setValue(["id":questionDto.id,"isBookMarked":questionDto.isBookMarked, "isCorrect":questionDto.isCorrect,"progressType":questionDto.progressType]) { (err, ref) in
            onUpdate?(questionDto)
            self.synced()
        }
       
    }
}
