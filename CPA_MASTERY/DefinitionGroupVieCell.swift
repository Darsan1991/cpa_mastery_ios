//
//  DefinitionGroupVieCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/29/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class DefinitionGroupViewCell:UITableViewCell{
    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var detailsLabel:UILabel!
    @IBOutlet weak var lockImgView:UIImageView!
    
    public private(set) var viewModel:DefinitionGroupTileUI!
    
    public override func awakeFromNib() {
        selectionStyle = .none
    }
    
    func configureCell(definitionGroupVM:DefinitionGroupTileUI){
        self.viewModel = definitionGroupVM
        self.titleLabel.text = definitionGroupVM.title
        self.detailsLabel.text = "\(definitionGroupVM.definitions.count) Definitions"
        lockImgView.isHidden = !definitionGroupVM.isLock
        
    }
    
    
}
