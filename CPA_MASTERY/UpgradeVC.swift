//
//  UpgradeVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/8/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class UpgradeVC: UIViewController,UpgradePageVCDelegate{

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var premiumGroup: UIView!
    @IBOutlet weak var freeGroup: UIView!
    @IBOutlet weak var freeIndicator: ViewD!
    @IBOutlet weak var premiumIndicator: ViewD!
    
    private var upgradeVC:UpgradePageVC!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        upgradePageVC(pageChanged: 0)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == UPGRADE_PAGE_SEGUE
        {
            upgradeVC = (segue.destination as! UpgradePageVC)
            upgradeVC.pageDelegate = self
        }
    }
    
    @IBAction func onClickIndicator(_ sender: Any) {
        
         let view = sender as! UIView
        upgradeVC.setTheCurrentIndex(index: view.tag == 0 ? 0 : 1)
        
    }
    
    
    func upgradePageVC(pageChanged index: Int) {
        premiumGroup.isHidden = index != 1
        freeGroup.isHidden = index != 0
        freeIndicator.backgroundColor = index == 0 ? UIColor.white : UIColor.clear
        premiumIndicator.backgroundColor = index == 1 ? UIColor.white : UIColor.clear
        
        titleLabel.text = index == 0 ? "Free" : "Premium"
        
    }

    @IBAction func onClickBuy(_ sender: Any) {
        let progress = ProgressHandler()
        progress.show()
        GlobalService.instance.accountHandler.unlockPremiumLock(unlock: true) { (sucess) in
            progress.dismiss()
            if sucess
            {
                self.performSegue(withIdentifier: LOADING_VC_SEGUE, sender: nil)
            }
        }
    }
    @IBAction func onCloseButton(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}
