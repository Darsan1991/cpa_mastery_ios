//
//  PopUpHandler.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/30/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class PopUpHandler:PopUpViewDelegate
{
    public private(set) var popUp:PopUpView?
    private var parentView:UIView!
    private var vc : UIViewController!
    
    init(rootView:UIView,vc:UIViewController) {
        
        parentView = rootView
        self.vc = vc
        
    }
    
    init(vc:UIViewController) {
       if let v = UIApplication.shared.keyWindow
       {
             parentView = v
        }
        self.vc = vc
    }
    
    
    
    func show()
    {
        if popUp == nil{
            popUp = PopUpView(frame: parentView.frame)
            popUp?.delegate = self
        }
        parentView.addSubview(popUp!)
    }
    
    func dismiss()
    {
        popUp?.removeFromSuperview()
    }
    
    public func onPositiveButtonClicked() {
        dismiss()
       showUpgrade(viewController: vc)
        
    }
    
    public func onNegativeButtonClicked() {
        dismiss()
    }
}
