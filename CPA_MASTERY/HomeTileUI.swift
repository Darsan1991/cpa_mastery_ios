//
//  HomeTileUI.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class HomeTileUI
{
    var iconImage:String!
    var title:String!
    var isPremium:Bool!
    var homeType:HomeType
    var extra:AnyObject?
    
    init(iconImage:String,title:String,isPremium:Bool,homeType:HomeType,extra:AnyObject?) {
        self.iconImage = iconImage
        self.title = title
        self.isPremium = isPremium
        self.homeType = homeType
        self.extra = extra
    }
    
    public enum HomeType
    {
        case SUBJECT
        case PROGRESS
        case DEFINITION
    }

}


