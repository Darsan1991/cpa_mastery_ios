//
//  ViewD.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/25/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

import UIKit

@IBDesignable class ViewD: UIView {
    
    @IBInspectable  var cornerRadius:CGFloat = 0 {
        didSet{
            setup()
        }
    }
    @IBInspectable var borderWidth:CGFloat = 0 {
        didSet{
            setup()
        }
    }
    
    @IBInspectable var borderColor:UIColor = UIColor.clear{
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowColor:UIColor = UIColor.clear{
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowOpacity:CGFloat = 0.5{
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowOffset:CGSize = CGSize.zero{
        didSet{
            setup()
        }
    }
    
    @IBInspectable var shadowRadius:CGFloat = 0{
        didSet{
            setup()
        }
    }

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    func setup(){
        layer.cornerRadius = cornerRadius
        layer.borderWidth = borderWidth
        layer.borderColor = borderColor.cgColor
        layer.shadowRadius = shadowRadius
        layer.shadowColor = shadowColor.cgColor
        layer.shadowOpacity = Float(shadowOpacity)
        layer.shadowOffset = shadowOffset
    }

    
}
