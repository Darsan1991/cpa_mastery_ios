//
//  ProgressViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/25/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ProgressViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel:UILabel!
    @IBOutlet weak var percentageLabel:UILabel!
    @IBOutlet weak var progressBar:MultiProgressBar!
    @IBOutlet weak var statusLabel:UILabel!
    
    private var viewModel:ProgressViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func configureCell(viewModel:ProgressViewModel)  {
        
        self.viewModel = viewModel
        titleLabel.text = viewModel.title
        percentageLabel.text = "\(Int(viewModel.correctlyAnswredPercentage))%"
        progressBar.setAdapterAndShowAllProgress(adapter: viewModel.questionAdapter)
        
        if viewModel.isWeak || viewModel.isStrong
        {
            statusLabel.isHidden = false
            statusLabel.text = viewModel.isWeak ? "WEAK" : "STRONG"
        }
        else{
            statusLabel.isHidden = true
        }
        
    }

}


public class ProgressViewModel
{
    var title:String!
    var questionAdapter:QuestionProgressAdapter!
    var correctlyAnswredPercentage:CGFloat
    {
        if totallyAnswred == 0
        {
            return 0
        }
        
        return 100 * CGFloat(answredCorrecty)/CGFloat(totallyAnswred)
    }
    
    public var isWeak:Bool{
        return totallyAnswred != 0 && answredCorrecty == 0
    }
    
    public var isStrong:Bool
    {
        return totallyAnswred != 0 && totallyAnswred == answredCorrecty
    }
    
    private var answredCorrecty:Int!
    private var totallyAnswred:Int!
    
    init(title:String,questionProgresses:[QuestionProgress]) {
        
        self.title = title
        questionAdapter = QuestionProgressAdapter(totalQuestions: questionProgresses)
        answredCorrecty = 0
        for question in questionProgresses
        {
            if question.isAnswredCorrectly
            {
                answredCorrecty = answredCorrecty + 1
            }
        }
        totallyAnswred = questionAdapter.totalQuestionList.count - questionAdapter.getQuestionCount(progressType: ProgressType.UN_ANSWRED)
    }
    
}
