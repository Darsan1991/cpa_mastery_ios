//
//  SelectionFilterTileDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol SelectionFilterTileDelegate
{
    func onClick(selectiongTile:SelectionFilterTile) -> Void
}
