//
//  ProgressAdapter.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation


public class ProgressAdapter
{
    public var totalQuestionList:[QuestionProgress]!
    public var typeVsProgressDict:Dictionary<ProgressType,[QuestionProgress]>!
    
    public var onDataChangedListners = [Callback]()
    
    
    init(totalQuestions:[QuestionProgress],onDataChanged:Callback? = nil) {
        totalQuestionList = totalQuestions
        if onDataChanged != nil
        {
            self.onDataChangedListners.append(onDataChanged!)
        }
        typeVsProgressDict = Dictionary<ProgressType,[QuestionProgress]>()
    }
    
    func addDatachangedListner(onDataChanged:@escaping Callback) {
        self.onDataChangedListners.append(onDataChanged)
    }
    
    func dataChanged()
    {
       for onChanged in self.onDataChangedListners
       {
            onChanged()
        }
    }
    
    func getTotalQuestionCount() -> Int {
        return totalQuestionList.count
    }
    
    func getTotalQuestions() -> [QuestionProgress]
    {
        return totalQuestionList;
    }
    
    func getQuestions(progressType:ProgressType) -> [QuestionProgress]
    {
        return typeVsProgressDict[progressType]!
    }
    
    func getQuestionCount(progressType:ProgressType) -> Int
    {
        return typeVsProgressDict[progressType]!.count
    }

    
    func removeAllDatachangedListners()
    {
        onDataChangedListners.removeAll()
    }
    
    func referesh()
    {

    }
    
}
