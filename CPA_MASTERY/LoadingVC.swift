//
//  LoadingVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/18/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class LoadingVC: UIViewController {

    private var accountHandler = FirebaseAccountHandler()

    
    private var questionService:QuestionProgressService?
    private var globalService:GlobalService?
    private var timer:Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        let _ =  InformationService.instance
        
        if !accountHandler.isAccountLogin()
        {
            DispatchQueue.main.async(execute: {
                self.performSegue(withIdentifier: "LogInVC", sender: nil)
            })
        }
        else
        {
            globalService = GlobalService.instance
            questionService =  QuestionProgressService.instance
            questionService?.inilizeOrReferesh()
            globalService?.inilizeOrReferesh()
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(checkForInilize), userInfo: nil, repeats: true)
            
            
        }

    }
    
    @objc private func checkForInilize()
    {
        if (self.questionService?.inilized)! && (self.globalService?.inilized)!
        {
            timer.invalidate()
            self.performSegue(withIdentifier: "MainVC", sender: nil)
        }
    }


}
