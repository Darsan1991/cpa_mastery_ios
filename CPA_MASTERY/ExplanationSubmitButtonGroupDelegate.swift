//
//  ExplanationSubmitButtonGroupDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/6/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

protocol ExplanationSubmitButtonGroupDelegate {
    
    func explanationSubmitButtonGroup(submitClicked progressType:ProgressType)->Void
}
