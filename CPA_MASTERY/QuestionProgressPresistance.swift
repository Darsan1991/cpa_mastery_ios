//
//  QuestionProgressPresistance.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol QuestionProgressPresistance
{
    typealias OnSync = ()->()
    typealias OnUpdate = (QuestionProgressDto)->()
    typealias OnLoadAll = ([QuestionProgressDto])->()
    typealias OnLoad = (QuestionProgressDto)->()
    
    var onSync:OnSync?{get set}
    
    var lastSynced:Date{get}
    func loadAllQuestions(onLoadAll:OnLoadAll?)
    
    func updateQuestionProgress(id:Int, questionDto:QuestionProgressDto, onUpdate:OnUpdate?)
    func loadQuestionProgress(id:Int,onLoadQuestion:OnLoad?)
    func reset(onTaskFinished:OnTaskFinished?)
    
}

