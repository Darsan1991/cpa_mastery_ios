//
//  QuizAnswerViewCell.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/3/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class QuizAnswerViewCell: UITableViewCell {

    @IBOutlet weak var answerLabel:UILabel!
    @IBOutlet weak var crossBtn:UIButton!
    @IBOutlet weak var checkView:ViewD!
    
    var delegate:QuizAnswerViewCellDelegate?
    
    var viewModel:QuizAnswerViewCellViewModel
    {
        set(newValue)
        {
            _viewModel = newValue
            configureCell(viewModel: _viewModel)
        }
        get{
            return _viewModel
        }
    }
    
    private var _viewModel:QuizAnswerViewCellViewModel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        // Initialization code
        answerLabel.text = "This is Answer This is Answer This is Answer This is Answer This is Answer This is Answer This is Answer This is Answer"
    }
    
    public enum Satate{
        case NONE
        case SELECTED
        case CROSSED
    }
    
    private func configureCell(viewModel:QuizAnswerViewCellViewModel)
    {
        setState(state: viewModel.state)
    }
    
    public func setState(state:Satate)
    {
        switch state {
        case .NONE:
            answerLabel.text = viewModel.answer
            setAlpha(alpha: 1)
            crossBtn.setTitle("X", for: .normal)
            answerLabel.font = UIFont.systemFont(ofSize: answerLabel.font.pointSize)
            checkView.backgroundColor = UIColor.clear
            break
            
        case .SELECTED:
            answerLabel.text = viewModel.answer
            setAlpha(alpha: 1)
            crossBtn.setTitle("X", for: .normal)
            checkView.backgroundColor = checkView.borderColor
            answerLabel.font = UIFont.boldSystemFont(ofSize: answerLabel.font.pointSize)
            break
            
        case .CROSSED:
            let attributeString: NSMutableAttributedString =  NSMutableAttributedString(string: viewModel.answer)
            attributeString.addAttribute(NSStrikethroughStyleAttributeName, value: 2, range: NSMakeRange(0, attributeString.length))
            answerLabel.attributedText = attributeString
            setAlpha(alpha: 0.7)
            crossBtn.setTitle("+", for: .normal)
            answerLabel.font = UIFont.systemFont(ofSize: answerLabel.font.pointSize)
            checkView.backgroundColor = UIColor.clear
        }
    }
    
    private func setAlpha(alpha:CGFloat)
    {
        answerLabel.alpha = alpha
        checkView.alpha = alpha
    }
    
    
    @IBAction func ontoggleClicked(_ sender:UIButton)
    {
        delegate?.quizAnswerViewCell(toggleCross: self)
    }

}

 class QuizAnswerViewCellViewModel
{
    var answer:String
    var state:QuizAnswerViewCell.Satate
    
    init(answer:String,state:QuizAnswerViewCell.Satate) {
        self.answer = answer
        self.state = state
    }
}


