//
//  QuizQuestionViewCellDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/24/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol QuizQuestionViewCellDelegate
{
    func quizQuestionViewCell(actionButton quizViewCell:QuizQuestionViewCell)->Void
}
