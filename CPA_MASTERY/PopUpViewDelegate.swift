//
//  PopUpViewDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/30/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public protocol PopUpViewDelegate{
    func onPositiveButtonClicked()
    func onNegativeButtonClicked()
}
