//
//  ExplanationTopViewCellDelegate.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/4/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

protocol ExplanationTopViewCellDelegate {
    func explanationTopViewCell(changed_segment index:Int)->Void
}
