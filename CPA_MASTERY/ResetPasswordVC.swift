//
//  ResetPasswordVC.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/17/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class ResetPasswordVC: UIViewController {
    
    @IBOutlet weak var emailTxtField: TextFieldD!
    @IBOutlet weak var resetButton: ButtonD!
    
    private var accountHandler:AccountHandler = FirebaseAccountHandler()
    private var progressHandler = ProgressHandler()

    override func viewDidLoad() {
        super.viewDidLoad()

         emailTxtField.addTarget(self, action: #selector(ResetPasswordVC.textFieldDidChange), for: UIControlEvents.editingChanged)
        // Do any additional setup after loading the view.
        
        //Fake call for Referesh
        textFieldDidChange()
    }


    @IBAction func onClickBack(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }

    @IBAction func onClickResetButton(_ sender: Any) {
        progressHandler.show()
        accountHandler.resetPassword(email: emailTxtField.text!) { (error) in
            self.progressHandler.dismiss()
            if error != nil {
                print("Error on reset password")
                
            }
            else{
                print("Reset password sent")

            }
        }
    }
    
    func textFieldDidChange()
    {
        if emailTxtField.text != nil && emailTxtField.text!.characters.count > 0
        {
            if !resetButton.isUserInteractionEnabled
            {
                resetButton.isUserInteractionEnabled = true
            }
        }
        else
        {
            if resetButton.isUserInteractionEnabled
            {
                resetButton.isUserInteractionEnabled = false
            }

        }
    }

    
}
