//
//  BookMarkProgressAdapter.swift
//  CPA_MASTERY
//
//  Created by Darsan on 5/22/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import Foundation

public class BookMarkProgressAdapter:ProgressAdapter{
    public private(set) var bookMarkQuestionList:[QuestionProgress]!
    
    override init(totalQuestions: [QuestionProgress], onDataChanged: Callback? = nil) {
        super.init(totalQuestions: totalQuestions,onDataChanged: onDataChanged)
        bookMarkQuestionList = [QuestionProgress]()
    
        for i in 0...3
        {
            typeVsProgressDict.updateValue([QuestionProgress](), forKey: ProgressType(rawValue: i)!)
        }
        
        referesh()
    }
    
    
    override func referesh()
    {
        clearDatas()
        
        for question in totalQuestionList
        {
            if question.isBookMarked
            {
                bookMarkQuestionList.append(question)
                typeVsProgressDict[question.progressType]?.append(question)
            }
        }
        
        dataChanged()
    }
    
    private func clearDatas()
    {
        bookMarkQuestionList.removeAll()
        for i in 0...3
        {
            typeVsProgressDict[ProgressType(rawValue:i)!]!.removeAll()
        }
    }
    
}
