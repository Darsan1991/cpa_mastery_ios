//
//  TextFieldD.swift
//  CPA_MASTERY
//
//  Created by Darsan on 6/16/17.
//  Copyright © 2017 fiverr. All rights reserved.
//

import UIKit

class TextFieldD: UITextField,UITextFieldDelegate {

    @IBInspectable var outerBorderWidth:CGFloat = 0{
        didSet{
            setup(editing: isEditing)
        }
    }
    
    @IBInspectable var normalOuterBorderColor:UIColor = UIColor.clear{
        didSet{
            setup(editing: isEditing)
        }
    }
    
    @IBInspectable var selectedOuterBorderColor:UIColor = UIColor.clear{
        didSet{
            setup(editing: isEditing)
        }
    }
    
    @IBInspectable var normalBackgroundColor:UIColor = UIColor.clear{
        didSet{
            setup(editing: isEditing)
        }
    }
    
    @IBInspectable var selectedBackgroundColor:UIColor = UIColor.clear{
        didSet{
            setup(editing: isEditing)
        }
    }
    
    @IBInspectable var normalTextColor:UIColor = UIColor.clear{
        didSet{
            setup(editing: isEditing)
        }
    }
    
    @IBInspectable var selectedTextColor:UIColor = UIColor.clear{
        didSet{
            setup(editing: isEditing)        }
    }
    
    @IBInspectable var activeAtHasText:Bool = true{
        didSet{
            setup(editing: isEditing)        }
    }

    
    @IBInspectable var padding:CGPoint = CGPoint.zero
        {
        didSet{
            
        }
    }
    
    @IBInspectable var cornerRadius:CGFloat = 0
    {
        didSet{
            setup(editing: isEditing)
        }
    }
    
    
    private var paddingText:UIEdgeInsets{
        return UIEdgeInsets(top: padding.y, left: padding.x, bottom: padding.y, right: padding.x)
    }
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup(editing: isEditing)
       delegate = self
    }
   
    
    override var delegate: UITextFieldDelegate?
    {
        set(val)
        {
            if val == nil
            {
                subDelegate = nil
            }
            
            if !val!.isKind(of: TextFieldD.self)
            {
                subDelegate = val
            }
            else
            {
                super.delegate = val
            }
        }
        
        get{
            return subDelegate
        }
        
    }
    
    private var subDelegate:UITextFieldDelegate?
    

    
    private func setup(editing:Bool)
    {
        let focused = editing || (activeAtHasText && text != nil && text!.characters.count > 0)
        layer.cornerRadius = cornerRadius
        layer.borderWidth = outerBorderWidth
        layer.borderColor = focused ? selectedOuterBorderColor.cgColor : normalOuterBorderColor.cgColor
        layer.backgroundColor = focused ? selectedBackgroundColor.cgColor : normalBackgroundColor.cgColor
        textColor = focused ? selectedTextColor : normalTextColor
        setValue(textColor, forKeyPath: "_placeholderLabel.textColor")
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, paddingText)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, paddingText)

    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, paddingText)
    }

    
    
    

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return subDelegate != nil && subDelegate!.textFieldShouldBeginEditing != nil ? subDelegate!.textFieldShouldBeginEditing!(textField) : true
    }
     public func textFieldDidBeginEditing(_ textField: UITextField) {
        setup(editing: true)
        subDelegate?.textFieldDidBeginEditing?(textField)
    }

     public func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return subDelegate != nil && subDelegate!.textFieldShouldEndEditing != nil ? subDelegate!.textFieldShouldEndEditing!(textField) : true
    }

     public func textFieldDidEndEditing(_ textField: UITextField) {
        setup(editing: false)
         subDelegate?.textFieldDidEndEditing?(textField)
    }

     public func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason)
     {
         setup(editing: false)
        subDelegate?.textFieldDidEndEditing?(textField, reason: reason)
    }
    
     public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool
    {
        return subDelegate != nil && subDelegate!.textField != nil ? subDelegate!.textField!(textField, shouldChangeCharactersIn: range, replacementString: string ): true
    }
    
    

     public func textFieldShouldClear(_ textField: UITextField) -> Bool
    {
        return subDelegate != nil && subDelegate!.textFieldShouldClear != nil ? subDelegate!.textFieldShouldClear!(textField) : true
    }

    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return subDelegate != nil && subDelegate!.textFieldShouldReturn != nil ? subDelegate!.textFieldShouldReturn!(textField) : true
    }
 
}
